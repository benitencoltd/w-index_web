<?php
if($deviceType == 'phone'){
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        setcookie('sp', true, time() + (86400 * 30), "/"); // 86400 = 1 day

        $current_url =  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

        $current_url =  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $escaped_url = htmlspecialchars( $current_url, ENT_QUOTES, 'UTF-8' );
        $redirect = str_replace($_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST'].'/sp', $escaped_url);
        $redirect = str_replace('.html', '.php', $redirect);
        $redirect = str_replace('index.php', '', $redirect);
        ?>
        <script type='text/javascript'>window.location.href = '<?php echo $redirect; ?>';</script>
        <?php
    }
    ?>
    <div class="pc">
        <div class='vcenter'>
            <a href="<?php echo $redirect ?>?sp=false" onclick="event.preventDefault();
                        document.getElementById('pc-form').submit();">
                <img src="/img/btn-sp.png" height="50px" />
            </a>

            <form id="pc-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" style="display: none;">
            </form>
        </div>
    </div>
<?php } ?>
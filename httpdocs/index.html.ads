<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>リアルタイム世界の株価指数と為替</title>
<meta name="description" content="世界各国の株式指標と為替の動きをリアルタイムで見られます。" />
<meta name="keywords" content="株価,株,株式,為替,ドル,指数,指標,世界,世界の株価,海外,リアルタイム,Dow,DJIA,ダウ,Nasdaq,ナスダック,アメリカ,ヨーロッパ,イギリス,フランス,ドイツ,イタリア,香港,ハンセン,FTSE,FTSE100,CAC,40,DAX,30,IBEX,ADR,CME,SGX,チャート,globex,グロベ,グローベックス" />
<link rel="stylesheet" href="/css/styles.css" type="text/css" />
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if(eregi('Mac', $ua)){
	echo('<link rel="stylesheet" href="/css/stylesm.css" type="text/css" />');
}
?>
<script type="text/javascript" src="/js/js.js"></script>
<link rel="canonical" href="http://www.w-index.com/" />
<link rel="shortcut icon" href="/img/favicon.ico" />
<meta name="format-detection" content="telephone=no" />

<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'ja'}
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36649344-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<!-- <?php
require_once("/var/www/vhosts/w-index.com/private/dbinfo.php");
$s=mysql_connect($serv,$user,$pass) or die("取得失敗");
mysql_select_db($dbnm);
mysql_query("set names utf8");
?> -->
<body onload ="setInterval('update_images()',1000*60);">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="container">
<div id="header">
<a href="http://www.w-index.com/"><img src="/img/logo.png" id="himg" width="241" height="28" alt="リアルタイム世界の株価指数と為替" /></a>
<ul id="headerright">
<li><b>S&amp;P500</b>：<?php
$re = mysql_query("select last,chan from indexquote where id='3'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span>";}
else{echo "<span class=\"green\">+$y[1]</span>";}
}
?></li>
<li><b>WTI原油</b>：<?php
$re = mysql_query("select last,chan from comquote where id='2'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span>";}
else{echo "<span class=\"green\">+$y[1]</span>";}
}
?></li>
<li><b>金(Gold)</b>：<?php
$re = mysql_query("select last,chan from comquote where id='6'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span>";}
else{echo "<span class=\"green\">+$y[1]</span>";}
}
?></li>
<li><a href="/old/">旧デザインにする</a></li>
</ul>
</div><!-- header -->

<div id="bigbanner">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:header_728x90 */
google_ad_slot = "4499058204";
google_ad_width = 728;
google_ad_height = 90;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<noscript>
<a href="http://twitter.com/w_index" target="_blank"><img src="/img/tw72890.png" width="728" height="90" alt="世界の株価Twitter" /></a>
</noscript>
</div>

<div id="navi">
<ul>
<li><a href="/" class="select">トップ</a></li>
<li><a href="/night.html">夜間用</a></li>
<li><a href="/world/">外国株</a></li>
<li><a href="/forex/">外国為替</a></li>
<li><a href="/commodity/">商品相場</a></li>
<li><a href="/225/">日本株</a></li>
<li><a href="/bond/">国債</a></li>
<li><a href="/news/">ニュース</a></li>
<li><a href="/hikaku/">証券比較</a></li>
</ul>

<form action="http://www.w-index.com/result/" id="cse-search-box">
  <div>
    <input type="hidden" name="cx" value="partner-pub-6205980071848979:1545591800" />
    <input type="hidden" name="cof" value="FORID:10" />
    <input type="hidden" name="ie" value="UTF-8" />
    <input type="text" name="q" size="22" />
    <input type="submit" name="sa" value="検索" />
  </div>
</form>
<script type="text/javascript" src="http://www.google.co.jp/coop/cse/brand?form=cse-search-box&amp;lang=ja"></script>

</div><!-- navi -->

<h1>【世界の株価指数】</h1>
<div id="tops">
<div class="nydow">
<h3>NYダウ30</h3>
<a href="http://w-index.org/cimg/daily/dow30.gif" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/dow30.gif" width="175" height="159" class="imup" alt="NYダウチャート" /></a>
</div>
<div class="nydow">
<h3>NASDAQ</h3>
<a href="http://w-index.org/cimg/daily/nasdaq.gif" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/nasdaq.gif" width="175" height="159" class="imup" alt="NASDAQチャート" /></a>
</div>
<div class="usdjpy">
<h3>米ドル/円</h3>
<a href="http://w-index.org/cimg/daily/usdjpy.gif" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/usdjpy.gif" width="172" height="114" class="imup" alt="USD/JPYチャート" /></a>
</div>
<div class="usdjpy">
<h3>ユーロ/米ドル</h3>
<a href="http://w-index.org/cimg/daily/eurusd.gif" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/eurusd.gif" width="172" height="114" class="imup" alt="EUR/USDチャート" /></a>
</div>
<div class="pickup">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:right_rect_200x200 */
google_ad_slot = "8929257805";
google_ad_width = 200;
google_ad_height = 200;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<noscript>
<p>当サイトは一部にJavaScriptを使用しています。<br />
Javascriptをオフにされている方はお手数ですが、手動で最新の情報に更新して下さい。<br />
F5キーでページを更新することができます。</p>
</noscript>
</div><!-- pickup -->
</div><!-- tops -->

<div id="main">
<div class="wchart">
<h3>日経225</h3>
<b id="i-4"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='4'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/n225.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/n225.png" width="190" height="95" class="imup" alt="日経平均株価チャート" /></a>
</div>
<div class="wchart">
<h3>韓国 KOSPI</h3>
<b id="i-5"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='5'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/ks11.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/ks11.png" width="190" height="95" class="imup" alt="韓国KOSPIチャート" /></a>
</div>
<div class="wchart">
<h3>中国 上海総合</h3>
<b id="i-6"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='6'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/shanghai.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/shanghai.png" width="190" height="95" class="imup" alt="中国上海総合指数チャート" /></a>
</div>
<div class="wchart">
<h3>台湾 加権</h3>
<b id="i-7"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='7'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/twii.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/twii.png" width="190" height="95" class="imup" alt="台湾加権チャート" /></a>
</div>
<div class="wchart">
<h3>香港　HANG SENG</h3>
<b id="i-8"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='8'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/hsi.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/hsi.png" width="190" height="95" class="imup" alt="香港HANGSENG指数チャート" /></a>
</div>
<div class="wchart">
<h3>シンガポール ST</h3>
<b id="i-9"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='9'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/sti.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/sti.png" width="190" height="95" class="imup" alt="シンガポールSTチャート" /></a>
</div>
<div class="wchart">
<h3>オ-ストラリア ASX</h3>
<b id="i-10"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='10'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/aord.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/aord.png" width="190" height="95" class="imup" alt="オ-ストラリアASXチャート" /></a>
</div>
<div class="wchart">
<h3>インド SENSEX30</h3>
<b id="i-11"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='11'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/bsesn.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/bsesn.png" width="190" height="95" class="imup" alt="インドSENSEX30チャート" /></a>
</div>
<div class="wchart">
<h3>イギリス FTSE100</h3>
<b id="i-12"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='12'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/ftse.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/ftse.png" width="190" height="95" class="imup" alt="イギリスFTSE100チャート" /></a>
</div>
<div class="wchart">
<h3>フランス CAC40</h3>
<b id="i-13"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='13'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/cac40.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/cac40.png" width="190" height="95" class="imup" alt="フランスCAC40チャート" /></a>
</div>
<div class="wchart">
<h3>ドイツ DAX</h3>
<b id="i-14"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='14'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/dax.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/dax.png" width="190" height="95" class="imup" alt="ドイツDAXチャート" /></a>
</div>
<div class="wchart">
<h3>イタリア FTSE MIB</h3>
<b id="i-15"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='15'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/ftsemib.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/ftsemib.png" width="190" height="95" class="imup" alt="イタリアFTSEMIBチャート" /></a>
</div>
<div class="wchart">
<h3>スペイン IBEX35</h3>
<b id="i-16"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='16'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/ibex.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/ibex.png" width="190" height="95" class="imup" alt="スペインIBEX35チャート" /></a>
</div>
<div class="wchart">
<h3>オランダ AEX</h3>
<b id="i-17"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='17'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/aex.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/aex.png" width="190" height="95" class="imup" alt="オランダAEXチャート" /></a>
</div>
<div class="wchart">
<h3>カナダ S&amp;P TSX</h3>
<b id="i-18"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='18'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/gsptse.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/gsptse.png" width="190" height="95" class="imup" alt="カナダS&amp;PTSXチャート" /></a>
</div>
<div class="wchart">
<h3>メキシコ IPC</h3>
<b id="i-19"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='19'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/ipc.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/ipc.png" width="190" height="95" class="imup" alt="メキシコIPCチャート" /></a>
</div>
<div class="wchart">
<h3>ブラジル Bovespa</h3>
<b id="i-20"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='20'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/bovespa.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/bovespa.png" width="190" height="95" class="imup" alt="ブラジルBovespa指数チャート" /></a>
</div>
<div class="wchart">
<h3>アルゼンチン Merval</h3>
<b id="i-21"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='21'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/merval.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/merval.png" width="190" height="95" class="imup" alt="アルゼンチンMervalチャート" /></a>
</div>
<div class="wchart">
<h3>スイス SMI</h3>
<b id="i-22"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='22'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/smi.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/smi.png" width="190" height="95" class="imup" alt="スイスSMI指数チャート" /></a>
</div>
<div class="wchart">
<h3>ロシア RTSI</h3>
<b id="i-23"><?php
$re = mysql_query("select last,chan,ratio from indexquote where id='23'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/cimg/daily/rtsi.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/cimg/rtsi.png" width="190" height="95" class="imup" alt="ロシアRTSIチャート" /></a>
</div>
<div id="underbanner">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:footer_728x90 */
google_ad_slot = "7452524605";
google_ad_width = 728;
google_ad_height = 90;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<noscript>
<a href="/land/click-sec.html" target="_blank" rel="nofollow"><img src="http://www.accesstrade.net/at/r.html?rk=01002rnn007j86" width="728" height="90" alt="クリック証券" /></a>
</noscript>
</div>
</div><!-- main -->

<div id="subwaku">
<div class="sub">
<p class="pside">全て60秒間隔で自動更新します。現地時間 or U.S.東部時間表示です</p>
<p class="pside">チャートをクリックすると日足チャートが出ます。</p>
<ul class="side">
<li><a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="w_index" data-lang="ja">ツイート</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script></li>
<li><div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div></li>
<li><g:plusone size="medium"></g:plusone></li>
</ul>
</div>

<div class="sub">
<h3><a href="/outside/blank.html">CME GLOBEX</a></h3>
<ul class="side">
<li><a href="/minichart/ny-dow.html" onclick="open('/minichart/ny-dow.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">NYダウ先物</a></li>
<li><a href="/minichart/nasdaq.html" onclick="open('/minichart/nasdaq.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">NASDAQ先物</a></li>
<li><a href="/minichart/sp500.html" onclick="open('/minichart/sp500.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">S&amp;P500先物</a></li>
<li><a href="/minichart/cme-nikkei225.html" onclick="open('/minichart/cme-nikkei225.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">CME Nikkei225</a></li>
<li><a href="/minichart/cme-nikkei225-yen.html" onclick="open('/minichart/cme-nikkei225-yen.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">CME Nikkei225(円建て)</a></li>
<li><a href="/minichart/crude-oil.html" onclick="open('/minichart/crude-oil.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">WTI原油先物</a></li>
<li><a href="/minichart/gold.html" onclick="open('/minichart/gold.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">金(Gold)</a></li>
</ul>
</div>

<div class="sub">
<h3>主要指標</h3>
<ul class="side">
<li><a href="/minichart/sgx-nikkei225.html" onclick="open('/minichart/sgx-nikkei225.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">SGX Nikkei225</a></li>
<li><a href="/minichart/jgb.html" onclick="open('/minichart/jgb.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">日本国債(JGB)</a></li>
<li><a href="/minichart/vix.html" onclick="open('/minichart/vix.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">VIX指数</a></li>
<li><a href="/minichart/soxx.html" onclick="open('/minichart/soxx.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">SOXX半導体指数</a></li>
<li><a href="/minichart/crb.html" onclick="open('/minichart/crb.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">CRB指数</a></li>
</ul>
</div>
<div class="sub">
<h3><a href="/news/">ニュース</a></h3>
<ul class="side">
<li><a href="/outside/newsblank.html">ニュースサイト一覧</a></li>
</ul>
</div>

<div class="sub">
<div class="linkunit">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:right_link_120x90 */
google_ad_slot = "2743123400";
google_ad_width = 120;
google_ad_height = 90;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
</div>

<div class="tweet">
<a href="http://www.twitter.com/w_index" target="_blank"><img src="/img/follow-me.png" width="160" height="36" alt="w_indexをフォロー"/></a>
</div>


</div><!-- subwaku -->

<div id="footer">
<div id="copyright">&copy;2003-<?php echo date(Y); ?> <a href="/">リアルタイム世界の株価指数と為替</a></div>
<ul>
<li><a href="/old/">旧デザインにする</a></li>
<li><a href="/support/contact.html">お問い合わせ</a></li>
<li><a href="/support/media.html">広告掲載について</a></li>
<li><a href="/support/about.html">当サイトについて</a></li>
</ul>
</div><!-- footer -->
</div><!-- containar -->
<?php
mysql_close($s);
?>

</body>
</html>


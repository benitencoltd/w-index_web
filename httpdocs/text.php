<?php



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



if (!function_exists('json_encode')) {
	
    require_once 'data/Services_JSON-1.0.3/JSON.php';
	function json_encode($value) {
		$s = new Services_JSON();
		return $s->encodeUnsafe($value);
	}

	function json_decode($json, $assoc = false) {
		$s = new Services_JSON($assoc ? SERVICES_JSON_LOOSE_TYPE : 0);
		return $s->decode($json);
	}
}

echo '<br/>';
//indexquote
echo 'indexquote<br/>';
$i_sym = '"%5EIXIC","%5EGSPC","%5EN225","%5EKS11","000001.SS","%5ETWII","%5EHSI","%5ESTI","%5EAORD","%5EBSESN","%5EFTSE","%5EFCHI","%5EGDAXI","FTSEMIB.MI","%5EIBEX","%5EAEX","%5EGSPTSE","%5EMXX","%5EBVSP","%5EMERV","%5ESSMI","RTS.RS"';
//$indexquote_syms = explode(',', trim($i_sym,','));
//
//foreach($indexquote_syms as $indexquote_sym){
//    $q_indexquote[] = getData($indexquote_sym);
//}
$q_indexquote = getDataCSV($i_sym);

echo '<br/>';
//dow30quote
echo 'dow30quote<br/>';
$d_sym = '"NKE","AXP","BA","GS","CAT","CSCO","CVX","DD","DIS","GE","HD","V","IBM","INTC","JNJ","JPM","UNH","KO","MCD","MMM","MRK","MSFT","PFE","PG","T","TRV","UTX","VZ","WMT","XOM"';

$q_dow30quote = getDataCSV($d_sym);

echo '<br/>';
//usquote
echo 'usquote<br/>';
$u_sym = '"AAPL","GOOG","YHOO","EBAY","AMZN","ADBE","SYMC","ORCL","TXN","DELL","NVDA","AMD","C","GS","MS","BK","BRK-B","GM","F","SBUX"';

$q_usquote = getDataCSV($u_sym);

echo '<br/>';
//forexquote1
echo 'forexquote1<br/>';
$f1_sym = '"USDJPY=X","EURJPY=X","GBPJPY=X","AUDJPY=X","NZDJPY=X","CADJPY=X","CHFJPY=X"';

$q_forexquote1 = getDataCSV($f1_sym);

echo '<br/>';
//forexquote2
echo 'forexquote2<br/>';
$f2_sym = '"EURUSD=X","GBPUSD=X","AUDUSD=X","USDCHF=X","EURGBP=X","HKDJPY=X","CNYJPY=X","KRWJPY=X","THBJPY=X","SGDJPY=X","TRYJPY=X","EURDKK=X","EURRUB=X","EURRON=X","USDHKD=X","IQDJPY=X","USDIQD=X","NZDUSD=X","USDCAD=X","USDSGD=X","USDTRY=X","USDMXN=X","USDPLN=X","EURAUD=X","EURCAD=X","EURCHF=X","EURNOK=X","EURSEK=X","EURPLN=X","EURSGD=X","EURTRY=X","GBPAUD=X","GBPNZD=X","GBPCAD=X","GBPCHF=X","AUDNZD=X","AUDCAD=X","AUDCHF=X","EURNZD=X","USDCNY=X"';
$q_forexquote2 = getDataCSV($f2_sym);

echo '<br/>';
//adrquote_sym
echo 'adrquote_sym<br/>';
$a_sym = '"ATE","CAJ","DCM","HTHIY","HMC","IX","KNM","KUBTY","KYO","PCRFY","MFG","MTU","NJ","NMR","NTT","SNE","TM","IIJI","MITSY","MKTAY","WACLY"';

$q_adrquote = getDataCSV($a_sym);

echo '<br/>';
//otherquote
echo 'otherquote<br/>';
$o_sym = '"%5EIRX","%5EFVX","%5ETNX","%5ETYX","%5ERUT","%5EXMI","%5EVIX","%5ESOX","%5ESTOXX50E"';
$q_otherquote = getDataCSV($o_sym);

function getDataCSV($symbols){
        $symbol = str_replace('"', '', $symbols);
        $url = "http://download.finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1c1ohgv&h=0&s=".$symbol;
        
        $result = array();
        echo '<pre>';
        if (($handle = fopen($url, "r")) !== FALSE) {
            $res = new \stdClass();

            while (($data = fgetcsv($handle, 100, ",")) !== FALSE) {
                $hold['Symbol'] = $data[0];
                $hold['LastTradePriceOnly'] = $data[1];
                $hold['LastTradeDate'] = $data[2];
                $hold['LastTradeTime'] = $data[3];
                $hold['Change'] = $data[4];
                $hold['Open'] = $data[5];
                $hold['DaysHigh'] = $data[6];
                $hold['DaysLow'] = $data[7];
                $hold['Volume'] = $data[8];
                
                $result[]= json_decode (json_encode ($hold), FALSE);
            }
            fclose($handle);
        }
        print_r($result);
        
	return $result;
}

function getData($symbols){
        $url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quoteslist%20where%20symbol%3D'.$symbols.'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';
	//$url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20('.$symbols.')&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';
	$json = file_get_contents($url);
	$json_datas = json_decode($json);
	$data = $json_datas->query->results->quote;
	
	return $data;
}

?>




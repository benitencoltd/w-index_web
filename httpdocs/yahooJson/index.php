<?php
ini_set('display_errors', 1);

if (!function_exists('json_encode')) {
    require_once '../data/Services_JSON-1.0.3/JSON.php';
        function json_encode($value) {
            $s = new Services_JSON();
            return $s->encodeUnsafe($value);
        }

        function json_decode($json, $assoc = false) {
            $s = new Services_JSON($assoc ? SERVICES_JSON_LOOSE_TYPE : 0);
            return $s->decode($json);
        }
}

$get_symbol = $_GET['symbol'];
$q_index = strtolower($get_symbol);

$range = isset($_GET['range'])?$_GET['range']:'1d';

$json = $range=='1d'?'json':'m_json';

$str = file_get_contents('../data/yahoo_json/'.$json.'/'.$q_index.'.json');
echo $str;

?>
<?php
$active = isset($active)?$active:'';
?>
<ul>
    <li><a href="/" <?php echo $active == ''?'class="select"':'' ?> >トップ</a></li>
    <li><a href="/night.html" <?php echo $active == 'night'?'class="select"':'' ?>>夜間用</a></li>
    <li><a href="/world/" <?php echo $active == 'world'?'class="select"':'' ?>>外国株</a></li>
    <li><a href="/forex/" <?php echo $active == 'forex'?'class="select"':'' ?>>外国為替</a></li>
    <li><a href="/commodity/" <?php echo $active == 'commodity'?'class="select"':'' ?>>商品相場</a></li>
    <li><a href="/225/" <?php echo $active == '225'?'class="select"':'' ?>>日本株</a></li>
    <li><a href="/bond/" <?php echo $active == 'bond'?'class="select"':'' ?>>国債</a></li>
    <li><a href="/vcurrentcy/" <?php echo $active == 'vcurrentcy'?'class="select"':'' ?>>仮想通貨</a></li>
    <li><a href="/news/" <?php echo $active == 'news'?'class="select"':'' ?>>ニュース</a></li>
    <li><a href="/hikaku/" <?php echo $active == 'hikaku'?'class="select"':'' ?>>証券比較</a></li>
    <li><a href="/pro/" <?php echo $active == 'pro'?'class="select"':'' ?>>専門家Q&amp;A</a></li>
</ul>
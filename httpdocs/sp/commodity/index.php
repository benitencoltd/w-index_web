<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-commodity.js"></script>'
);

$active = 'commodity';
$reload_img=false;
include('../layout/header.php');
?>
<div id="main">
    <h2>エネルギー</h2>

<div align="center">

<table width="100%" border="0" cellspacing="2" cellpadding="2">

<tr>
<td>WTI原油先物</td>
<td align="right">
<b id="c-2"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='2'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td><a href="http://w-index.org/minichart/oil.png" class="highslide" onclick="return hs.expand(this)">分足</a></td>
<td><a href="http://w-index.org/commodity/daily/oil.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>灯油</td>
<td align="right">
<b id="c-3"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='3'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/heatingoil.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>ガソリン</td>
<td align="right">
<b id="c-4"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='4'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/gasoline.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>天然ガス</td>
<td align="right">
<b id="c-5"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='5'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/naturalgas.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

</table>

<h2>貴金属</h2>

<div class="linkunit">
<div align="center"><a href="/night.html">※貴金属とCRB指数のチャートは夜間用ページで一覧表示しています。</a></div>
</div>

<table width="100%" border="0" cellspacing="2" cellpadding="2">

<tr>
<td>金(Gold)</td>
<td align="right">
<b id="c-6"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='6'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td><a href="http://w-index.org/minichart/gold.png" class="highslide" onclick="return hs.expand(this)">分足</a></td>
<td><a href="http://w-index.org/commodity/daily/gold.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>銀(Silver)</td>
<td align="right">
<b id="c-7"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='7'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td><a href="http://w-index.org/minichart/silver.png" class="highslide" onclick="return hs.expand(this)">分足</a></td>
<td><a href="http://w-index.org/commodity/daily/silver.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>銅(Copper)</td>
<td align="right">
<b id="c-8"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='8'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td><a href="http://w-index.org/minichart/copper.png" class="highslide" onclick="return hs.expand(this)">分足</a></td>
<td><a href="http://w-index.org/commodity/daily/copper.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>プラチナ</td>
<td align="right">
<b id="c-9"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='9'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td><a href="http://w-index.org/minichart/platinum.png" class="highslide" onclick="return hs.expand(this)">分足</a></td>
<td><a href="http://w-index.org/commodity/daily/platinum.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

</table>

<h2>食料品</h2>

<table width="100%" border="0" cellspacing="5" cellpadding="5">


<tr>
<td>DJ-UBS商品指数</td>
<td align="right">
<b id="c-25"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='25'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td><a href="http://w-index.org/minichart/djc.png" class="highslide" onclick="return hs.expand(this)">分足</a></td>
<td><a href="http://w-index.org/cimg/daily/djc.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>トウモロコシ</td>
<td align="right">
<b id="c-11"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='11'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/corn.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>小麦</td>
<td align="right">
<b id="c-12"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='12'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/wheat.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>大豆</td>
<td align="right">
	<b id="c-13">
	<?php
	$re = mysql_query("select last,chan,ratio from comquote where id='13'");
	while($y=mysql_fetch_array($re)){
		echo "$y[0] ";
		if(0 > $y[2]){
			echo "<span class=\"red\">$y[1]($y[2]%)</span>";
		}
		else{
			echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";
		}
	}
	?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/soybeans.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>生牛</td>
<td align="right">
<b id="c-14"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='14'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/livecattle.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>赤身豚肉</td>
<td align="right">
<b id="c-15"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='15'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/leanhogs.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>ココア</td>
<td align="right">
<b id="c-16"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='16'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/cocoa.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>コーヒー</td>
<td align="right">
<b id="c-17"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='17'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/coffee.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>綿</td>
<td align="right">
<b id="c-18"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='18'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/cotton.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>

<tr>
<td>砂糖</td>
<td align="right">
<b id="c-19"><?php
$re = mysql_query("select last,chan,ratio from comquote where id='19'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[2]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
</td>
<td>分足</td>
<td><a href="http://w-index.org/commodity/daily/sugar.png" class="highslide" onclick="return hs.expand(this)">日足</a></td>
</tr>
</table>

<div class="linkunit" style="font-size:12px;">
<div align="center">一部指数の分足チャートは、閲覧需要が多ければ追加しますが、需要や市場での取引量も少ないと思いますので、<br />ウェブサーバーの負荷を削減する為、<a href="/outside/blank.html">CME Globex</a>で見ていただけると助かります。</div>
</div>

</div><!-- aligncenter -->
    
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
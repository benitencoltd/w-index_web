<?php if(in_array($active, $bottom_ad)){ ?>
<div style="clear:both;margin:10px 0;">
    <center>
    <!-- End of Google ad tag bottom -->
    <script type="text/javascript"><!--
    google_ad_client = "ca-pub-0933571704299623";
    /* w-index sp:com_300x250_3 */
    google_ad_slot = "3782345271/5151328662";
    google_ad_width = 300;
    google_ad_height = 250;
    //-->
    </script>
    <script type="text/javascript"
    src="//pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
    <!-- End of Google ad tag bottom -->
    </center>
</div>
<?php
}
?>
<div id="footer">
    <ul>
        <li><a href="/sp/support/contact.php">お問い合わせ</a></li>
        <li><a href="/sp/support/media.php">広告掲載について</a></li>
        <li><a href="/sp/support/about.php">当サイトについて</a></li>
    </ul>
    <div id="copyright">　©2003-2017  リアルタイム世界の株価指数と為替</div>
</div><!-- footer -->
</div><!-- containar -->


<!--<div class="fix-clear"></div>-->

<?php
mysql_close($s);
?>
<!-- Latest compiled and minified JavaScript -->
<div class='tap-graph' id='tap-graph'>
    <div class="graph">
        <div class='graph-desc'>
            <h3></h3>
            <div class='info'><p></p></div>
        </div>
        <div class="graph-img">
            <div id='chart'></div>
        </div>
        <div class="graph-btn">
            ×
        </div>
        <div class="graph-adv">
            <?php echo '<center>'.$middle_ad.'</center>'; ?>
        </div>
    </div>
</div>
<p id="back-to-top">
    <img src='<?php echo $base_href.'/images/page-top.png'?>' width='30px;'>
</p>
<!-- Swiper JS -->
<script src="<?php echo $url?>/sp/js/swiper/swiper.min.js"></script>
<script>
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});
$(document).ready(function(){
    
    $('#back-to-top').click(function () {
        $('#back-to-top').hide();
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $(".tap-graph .graph-btn").click(function(){
        $(".tap-graph").hide();
//        $("body").css('overflow','initial');
//        $("body").css('position','relative');
        
        $('header').css({'position':'relative','top':'0'})
          
    })
    $(".wchart a").click(function(e){
        e.stopPropagation();
    });
    
    $(".fullChart").click(function(){
        var symbol = $(this).attr('data-symbol');
        var h3 = $(this).find('h3').html();
        var img = $(this).find('a').attr('href');
        var last = $(this).find('#last').val();
        var b = $(this).find('b').html();
        
        var position = $(this).position();
        
        
        $('header').css({'position':'fixed','top':'0'})
        
        
        $('.tap-graph .graph .graph-desc h3').html(h3);
        $('.tap-graph .graph .graph-desc .info p').html(b);
        $('.tap-graph .graph .graph-img img').attr('src',img);
        $('.tap-graph').css('display','table');
        
        YAHOO.JP.fin.common.drawIncChart("chart", symbol, "1mo", "n");
//        $("body").css('overflow','hidden');
//        $("body").css('position','fixed');
    });
    $(".fullImage").click(function(e){
        e.preventDefault();
       
        var href = $(this).attr('href');
        var img = href;
        
        $('.tap-graph .graph .graph-img #chart').html('<img src="'+img+'" width="100%">');
        
        $('.tap-graph').css('display','table');
        
//        $("body").css('overflow','hidden');
//        $("body").css('position','fixed');
        $('header').css({'position':'fixed','top':'0'})
        
        return false; 
    });
    
    var inislide = $(".sub-menu ul li a.active").attr('data-index');
   
    var swiper = new Swiper('.swiper-container', {
       // pagination: '.swiper-pagination',
        slidesPerView:3,
        paginationClickable: true,
        spaceBetween:0,
        initialSlide: inislide,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
});

</script>
</body>
</html>
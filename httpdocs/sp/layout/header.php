<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    setcookie('sp', false, time() + (86400 * 30), "/"); // 86400 = 1 day
    
    $urls =  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

    $escaped_url = htmlspecialchars( $urls, ENT_QUOTES, 'UTF-8' );
    $redirect = str_replace('/sp', '', $escaped_url);
    $redirect = str_replace('index.php', '', $redirect);
    $redirects = str_replace('.php', '.html', $redirect);
   
    ?>
    <script type='text/javascript'>window.location.href = '<?php echo $redirects; ?>';</script>
    <?php
}
error_reporting(0);
//require_once("/var/www/vhosts/w-index.com/private/dbinfo.php");
$url = isset($url)?$url:'../';
//require_once("/var/www/vhosts/w-index.com/private/dbinfo_sp.php");
require_once($url."../private/dbinfo_sp.php");

$s = mysql_connect($serv, $user, $pass) or die("取得失敗");
mysql_select_db($dbnm);
mysql_query("set names utf8");
$base_href = $url.'sp/';

$active=isset($active)?$active:'';

$noPc = true;
if($active =='minichart1' || $active =='minichart2' || $active == 'vcurrentcy'){
    $noPc = false;
}
$sub_active=isset($sub_active)?$sub_active:'';


$current_url =  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

$escaped_url = htmlspecialchars( $current_url, ENT_QUOTES, 'UTF-8' );
$redirect = str_replace('/sp', '', $escaped_url);

$redirect = str_replace('.php', '.html', $redirect);

$top_ad = array(
    'vcurrentcy',
    'night',
    'word',//world
    'forex',
    '225',
    'top'
    );
$bottom_ad = array(
    'bond',
    'minichart1',
    'minichart2',
    'vcurrentcy',
    'night',
    'word',//world
    'forex',
    'commodity',
    '225',
    'support',
    'top'
    );

$middle_ad ='<!--Google ad tag middle-->
<script type="text/javascript"><!--
google_ad_client = "ca-pub-0933571704299623";
/* w-index sp:com_300x250_2 */
google_ad_slot = "3782345271/6464410965";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<!-- End of Google ad tag middle-->';
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <meta http-equiv="Content-Script-Type" content="text/javascript" />
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes">
        <title>リアルタイム世界の株価指数と為替</title>
        <meta name="description" content="世界各国の株式指標と為替の動きをリアルタイムで見られます。" />
        <meta name="keywords" content="株価,株,株式,為替,ドル,指数,指標,世界,世界の株価,海外,リアルタイム,Dow,DJIA,ダウ,Nasdaq,ナスダック,アメリカ,ヨーロッパ,イギリス,フランス,ドイツ,イタリア,香港,ハンセン,FTSE,FTSE100,CAC,40,DAX,30,IBEX,ADR,CME,SGX,チャート,globex,グロベ,グローベックス" />
        
        <!-- Latest compiled and minified CSS -->
        <link rel="canonical" href="http://www.w-index.com/" />
        <link rel="shortcut icon" href="img/favicon.ico" />
        <meta name="format-detection" content="telephone=no" />
       
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
            {
                lang: 'ja'
            }
        </script>

        <script type="text/javascript">
            
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36649344-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <link rel="stylesheet" href="<?php echo $url?>sp/js/swiper/swiper.min.css">
        <script type="text/javascript" src="<?php echo $url?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $url?>js/chart-min.js"></script>
        <script type="text/javascript" src="<?php echo $url?>js/libra.js"></script>
        <?php 
        if(isset($js_script) && is_array($js_script) && count($js_script) > 0){
            foreach($js_script as $script){
                echo $script;
            }
        }
        ?>
        <link rel="stylesheet" href="<?php echo $url ?>sp/css/styles.css" type="text/css" />
    </head>
<?php $reload_img = isset($reload_img)?$reload_img:true; ?>
<body <?php if($reload_img !=false) { ?> onload="setInterval('update_images()',5000*60)" <?php } ?>>
    <div id="container">
        <header>
            <div class="logo">
                <div class='vcenter'>
                    <img src="<?php echo $base_href.'images/w-index-logo.png'?>" height="55px" />
                </div>
            </div>
            <?php 
            if($noPc){ ?>
            
            <div class="pc">
                <div class='vcenter'>
                    <a href="<?php echo $redirect ?>?sp=true" onclick="event.preventDefault();
                                document.getElementById('pc-form').submit();">
                        <img src="<?php echo $base_href.'images/btn-pc.png'?>" height="30px" />
                    </a>
                
                    <form id="pc-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" style="display: none;">
                       
                    </form>
                </div>
            </div>
            <?php } ?>
        </header>
        <nav class="menu">
            <ul class="main-menu">
                <li><a href="<?php echo $base_href; ?>" class='top-menu'><span>Top</span></a></li>
                <li><a href="<?php echo $base_href; ?>bond/index.php" <?php echo $active=='bond'?'class="active"':'';?>><span>国債</span></a></li>
                <li><a href="<?php echo $base_href; ?>minichart/ny-dow.php" <?php echo $active=='minichart1'?'class="active"':'';?>><span>CME GLOBEX</span></a></li>
                <li><a href="<?php echo $base_href; ?>minichart/sgx-nikkei225.php" <?php echo $active=='minichart2'?'class="active"':'';?>><span>主要指標</span></a></li>
                <li><a href="<?php echo $base_href; ?>vcurrentcy/index.php" <?php echo $active=='vcurrentcy'?'class="active"':'';?>><span>仮想通貨</span></a></li><!-- new category -->
            </ul>
            <ul class="main-menu clear">
                <li><a href="<?php echo $base_href; ?>night.php" <?php echo $active=='night'?'class="active"':'';?>><span>夜間用</span></a></li>
                <li><a href="<?php echo $base_href; ?>world/index.php" <?php echo $active=='word'?'class="active"':'';?>><span>外国株</span></a></li>
                <li><a href="<?php echo $base_href; ?>forex/index.php" <?php echo $active=='forex'?'class="active"':'';?>><span>外国為替</span></a></li>
                <li><a href="<?php echo $base_href; ?>commodity/index.php" <?php echo $active=='commodity'?'class="active"':'';?>><span>商品相場</span></a></li>
                <li><a href="<?php echo $base_href; ?>225/index.php" <?php echo $active=='225'?'class="active"':'';?>><span>日本株</span></a></li>
            </ul>
            
            <div class="<?php echo $active !='bond'?'hide':'sub-menu';?>">
                <ul>
                    <li><a href="<?php echo $base_href; ?>word/index.php" <?php echo $sub_active=='word'?'class="active"':'';?>><span>長期国債</span></a></li>
                </ul>
            </div>
            <!-- CME GLOBEX -->
            <div class="<?php echo $active !='minichart1'?'hide':'sub-menu swiper-container';?>">
                <ul class="swiper-wrapper">
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/ny-dow.php" <?php echo $sub_active=='ny-dow'?'class="active"':'';?> data-index="0"><span>NYダウ先物</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/nasdaq.php" <?php echo $sub_active=='nasdaq'?'class="active"':'';?> data-index="1"><span>NASDAQ先物</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/sp500.php" <?php echo $sub_active=='sp500'?'class="active"':'';?> data-index="2"><span>S&P500先物</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/cme-nikkei225.php" <?php echo $sub_active=='cme-nikkei225'?'class="active"':'';?> data-index="3"><span>CME Nikkei225</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/cme-nikkei225-yen.php" <?php echo $sub_active=='cme-nikkei225-yen'?'class="active"':'';?> data-index="4"><span>CME Nikkei225(円建て)</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/daw-cfd.php" <?php echo $sub_active=='daw-cfd'?'class="active"':'';?> data-index="5"><span>ダウ平均CFD</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/nikkei-cfd.php" <?php echo $sub_active=='nikkei-cfd'?'class="active"':'';?> data-index="6"><span>日経平均CFD</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/crude-oil.php" <?php echo $sub_active=='crude-oil'?'class="active"':'';?> data-index="7"><span>WTI原油先物</span></a></li>
                    <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/gold.php" <?php echo $sub_active=='gold'?'class="active"':'';?> data-index="8"><span>金(Gold)</span></a></li>
                </ul>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <!-- 主要指標 -->
            <div class="<?php echo $active !='minichart2'?'hide':'sub-menu swiper-container';?>">
            <ul class="swiper-wrapper">
                <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/sgx-nikkei225.php" <?php echo $sub_active=='minichart'?'class="active"':'';?> data-index="0"><span>SGX Nikkei225</span></a></li>
                <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/jgb.php" <?php echo $sub_active=='jgb'?'class="active"':'';?> data-index="1"><span>日本国債(JGB)</span></a></li>
                <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/vix.php" <?php echo $sub_active=='vix'?'class="active"':'';?> data-index="2"><span>VIX指数</span></a></li>
                <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/soxx.php" <?php echo $sub_active=='soxx'?'class="active"':'';?> data-index="3"><span>SOXX半導体指数</span></a></li>
                <li class="swiper-slide"><a href="<?php echo $base_href; ?>minichart/daw_ubs.php" <?php echo $sub_active=='daw_ubs'?'class="active"':'';?> data-index="4"><span>DJ-UBS商品指数</span></a></li>
            </ul>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
             <!-- 外国株 -->
            <div class="<?php echo $active !='word'?'hide':'sub-menu';?>">
                <ul>
                    <li><a href="<?php echo $base_href; ?>world/index.php" <?php echo $sub_active=='word'?'class="active"':'';?>><span>NYダウ30銘柄</span></a></li>
                    <li><a href="<?php echo $base_href; ?>world/us-stocks.php" <?php echo $sub_active=='us-stocks'?'class="active"':'';?>><span>米国主要銘柄</span></a></li>
                    <li><a href="<?php echo $base_href; ?>world/adr.php" <?php echo $sub_active=='adr'?'class="active"':'';?>><span>日本株ADR</span></a></li>
                </ul>
            </div>
             <!-- 外国為替 -->
            <div class="<?php echo $active !='forex'?'hide':'sub-menu';?>">
                <ul>
                    <li><a href="<?php echo $base_href; ?>forex/index.php" <?php echo $sub_active=='forex'?'class="active"':'';?>><span>メジャー通貨</span></a></li>
                    <li><a href="<?php echo $base_href; ?>forex/minor.php" <?php echo $sub_active=='minor'?'class="active"':'';?>><span>マイナー通貨</span></a></li>
                    <li><a href="<?php echo $base_href; ?>forex/jpy.php" <?php echo $sub_active=='jpy'?'class="active"':'';?>><span>日本円</span></a></li>
                    <li><a href="<?php echo $base_href; ?>forex/usd.php" <?php echo $sub_active=='usd'?'class="active"':'';?>><span>米ドル</span></a></li>
                    <li><a href="<?php echo $base_href; ?>forex/eur.php" <?php echo $sub_active=='eur'?'class="active"':'';?>><span>ユーロ</span></a></li>
                    <li><a href="<?php echo $base_href; ?>forex/gbp.php" <?php echo $sub_active=='gbp'?'class="active"':'';?>><span>ポンド</span></a></li>
                    <li><a href="<?php echo $base_href; ?>forex/aud.php" <?php echo $sub_active=='aud'?'class="active"':'';?>><span>豪ドル</span></a></li>
                </ul>
            </div>
             <!-- 商品相場 -->
             <div class="<?php echo $active !='commodity'?'hide':'sub-menu';?>">
            <ul>
                <li><a href="<?php echo $base_href; ?>commodity/" <?php echo $sub_active=='commodity'?'class="active"':'';?>><span>商品相場</span></a></li>
            </ul>
                 </div>
             <!-- 日本株 -->
             <div class="<?php echo $active !='sssbond'?'hide':'sub-menu';?>">
                <ul>
                    <li><a href="<?php echo $base_href; ?>" <?php echo $sub_active=='bond'?'class="active"':'';?>><span>日本株</span></a></li>
                </ul>
             </div>

        </nav><!-- /. menu -->
        
        <?php if(in_array($active, $top_ad)){ ?>
        <center>
        <!-- Google ad tag top-->
        <script type="text/javascript"><!--
        google_ad_client = "ca-pub-0933571704299623";
        /* w-index sp:com_300x250_1 */
        google_ad_slot = "3782345271/2469262968";
        google_ad_width = 300;
        google_ad_height = 250;
        //-->
        </script>
        <script type="text/javascript"
        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
        <!-- End of Google ad tag top -->
        </center>
        <?php
        }
        ?>
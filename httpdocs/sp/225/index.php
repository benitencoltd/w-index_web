<?php
$url = '../../';
$js_script = array(
    //'<script type="text/javascript" src="../js/js-225.js"></script>'
);

$active = '225';
$sub_active = '225';

include('../layout/header.php');
?>
<div id="main">
    <!--<div align="center"><a href="denki1.html">日経225 個別銘柄チャートはこちら</a></div>-->
    <div class='grid'>
        <div class="wchart-225">
            <h3><a href="/sp/225/minichart/suisan.html">水産・農林業</a></h3>
            <a href="http://w-index.org/225/daily/suisan.gif" class="fullImage" id="fullImage">
                <img src="http://w-index.org/225/daily/suisan.gif" class="imup" alt="" />
            </a>
        </div>
    </div>
    <div class='grid'>
        <div class="wchart-225">
            <h3><a href="/sp/225/minichart/kougyou.html">鉱業</a></h3>
            <a href="http://w-index.org/225/daily/kougyou.gif" class="fullImage" id="fullImage" >
                <img src="http://w-index.org/225/daily/kougyou.gif"  class="imup" alt="" />
            </a>
        </div>
        
    </div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kensetu.html">建設業</a></h3>
        <a href="http://w-index.org/225/daily/kensetu.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kensetu.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/shokuryou.html">食料品</a></h3>
        <a href="http://w-index.org/225/daily/shokuryouhin.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/shokuryouhin.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/senni.html">繊維製品</a></h3>
        <a href="http://w-index.org/225/daily/senni.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/senni.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kami.html">パルプ・紙</a></h3>
        <a href="http://w-index.org/225/daily/kami.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kami.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kagaku.html">化学</a></h3>
        <a href="http://w-index.org/225/daily/kagaku.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kagaku.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/iyaku.html">医薬品</a></h3>
        <a href="http://w-index.org/225/daily/iyakuhin.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/iyakuhin.gif"  class="imup" alt="" /></a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/sekiyu.html">石油・石炭製品</a></h3>
        <a href="http://w-index.org/225/daily/sekiyusekitan.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/sekiyusekitan.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/gomu.html">ゴム製品</a></h3>
        <a href="http://w-index.org/225/daily/gomu.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/gomu.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <!-- advertise-->
    <div style="clear:both">
        <center><?php echo $middle_ad ?></center>    
    </div
   <!-- /. advertise-->
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/garasu.html">硝子・土石製品</a></h3>
        <a href="http://w-index.org/225/daily/garasu.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/garasu.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/tetu.html">鉄鋼</a></h3>
        <a href="http://w-index.org/225/daily/tekkou.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/tekkou.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/hitetu.html">非鉄金属</a></h3>
        <a href="http://w-index.org/225/daily/hitetu.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/hitetu.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kinzoku.html">金属製品</a></h3>
        <a href="http://w-index.org/225/daily/kinnzoku.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kinnzoku.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kikai.html">機械</a></h3>
        <a href="http://w-index.org/225/daily/kikai.gif" class="fullImage" id="fullImage">
            <img src="http://w-index.org/225/daily/kikai.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/denki1.html">電気機器</a></h3>
        <a href="http://w-index.org/225/daily/denki.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/denki.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/yusou.html">輸送用機器</a></h3>
        <a href="http://w-index.org/225/daily/yusou.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/yusou.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/seimitu.html">精密機器</a></h3>
        <a href="http://w-index.org/225/daily/seimitu.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/seimitu.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/sonota.html">その他製品</a></h3>
        <a href="http://w-index.org/225/daily/sonota.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/sonota.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/denkigasu.html">電気・ガス業</a></h3>
        <a href="http://w-index.org/225/daily/denkigasu.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/denkigasu.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
   <!-- advertise-->
    <div style="clear:both">
        <center><?php echo $middle_ad ?></center>    
    </div
   <!-- /. advertise-->
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/rikuun.html">陸運業</a></h3>
        <a href="http://w-index.org/225/daily/rikuun.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/rikuun.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kaiun.html">海運業</a></h3>
        <a href="http://w-index.org/225/daily/kaiun.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kaiun.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kuuun.html">空運業</a></h3>
        <a href="http://w-index.org/225/daily/kuuun.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kuuun.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/souko.html">倉庫・運輸関連業</a></h3>
        <a href="http://w-index.org/225/daily/souko.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/souko.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/tuusin.html">情報・通信業</a></h3>
        <a href="http://w-index.org/225/daily/jyouhoutuusin.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/jyouhoutuusin.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/oroshi.html">卸売業</a></h3>
        <a href="http://w-index.org/225/daily/orosiuri.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/orosiuri.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/kouri.html">小売業</a></h3>
        <a href="http://w-index.org/225/daily/kouri.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/kouri.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/ginkou.html">銀行業</a></h3>
        <a href="http://w-index.org/225/daily/ginkou.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/ginkou.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/shouken.html">証券、商品先物取引業</a></h3>
        <a href="http://w-index.org/225/daily/shouken.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/shouken.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/hoken.html">保険業</a></h3>
        <a href="http://w-index.org/225/daily/hoken.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/hoken.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
    <h3><a href="/sp/225/minichart/kinyuu.html">その他金融業</a></h3>
    <a href="http://w-index.org/225/daily/kinyuu.gif" class="fullImage" id="fullImage" >
        <img src="http://w-index.org/225/daily/kinyuu.gif"  class="imup" alt="" />
    </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/fudousan.html">不動産業</a></h3>
        <a href="http://w-index.org/225/daily/fudousan.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/fudousan.gif"  class="imup" alt="" />
        </a>
    </div>
</div>
    <div class='grid'>
    <div class="wchart-225">
        <h3><a href="/sp/225/minichart/service.html">サービス業</a></h3>
        <a href="http://w-index.org/225/daily/service.gif" class="fullImage" id="fullImage" >
            <img src="http://w-index.org/225/daily/service.gif"  class="imup" alt="" />
        </a>
    </div>
        </div>
    
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
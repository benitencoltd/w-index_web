<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-dow30.js"></script>'
);

$active = 'word';
$sub_active = 'adr';

$reload_img = "";

include('../layout/header.php');
?>
<?php
$data=array();

$adrquote_title=array(
    17  => 'トヨタ自動車',
    5   => 'ホンダ',
    16  => 'ソニー',
    2   => 'キヤノン',
    10  => 'パナソニック',
    4   => '日立',
    9   => '京セラ',
    13  => '日本電産',
    12  => '三菱UFJ FG',
    11  => 'みずほFG',
    14  => '野村ホールディングス',
    6   => 'オリックス',
    15  => 'NTT',
    3   => 'NTTドコモ',
    8   => 'クボタ',
    1   => 'アドバンテスト',
    20  => 'マキタ',
    7   => 'コナミ',
    18  => 'インターネットイニシアティブ',
    21  => 'ワコール'
);

$i=0;
$re = mysql_query("SELECT * FROM adrquote WHERE id IN (17,5,16,2,10,4,9,13,12,11,14,6,15,3,8,1,20,7,18,21) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $y['div']='a';
    $id=$y['id'];
    $title =$adrquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}


function getData($data){
    $result['detail']='';
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];
    $result['title']=$data['title'];

    $k = number_format($data['last'],0);
    $l = number_format($data['chan'],0);

    if($data['id'] == 17 || $data['id'] == 11 ){
        $k = number_format(($data['last'] / 2),0);
        $l = number_format(($data['chan'] / 2),0);

    }else if($data['id'] == 4){
        $k = number_format(($data['last'] / 10),0);
        $l = number_format(($data['chan'] / 10),0);

    }
    else if($data['id'] == 8 || $data['id'] == 21){
        $k = number_format(($data['last'] / 5),0);
        $l = number_format(($data['chan'] / 5),0);
    }
    else if($data['id'] == 13){

        $k = number_format(($data['last'] * 4),0);
        $l = number_format(($data['chan'] * 4),0);
    }
    else if($data['id'] == 6 || $data['id'] == 15){
        $k = number_format(($data['last'] * 2),0);
        $l = number_format(($data['chan'] * 2),0);
    }
    else if($data['id'] == 3){
        $k = number_format(($data['last'] * 10),0);
        $l = number_format(($data['chan'] * 10),0);
    }
    else if($data['id'] == 18){
        $k = number_format(($data['last'] * 400),0);
        $l = number_format(($data['chan'] * 400),0);
    }

    $result['last'] = "$k"."円 ";
    $result['div'] = $data['div'];

    if($data['chan'] > 0){
        $result['detail'] .= "<span class='green'>+".$l."(+".$data['ratio']."%)</span>";
    }else{
        $result['detail'] .= "<span class='red'>".$l."(".$data['ratio']."%)</span>";
    }

    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
    foreach($data as $key=>$value){
        echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
                echo $value['detail'];
            echo '</b>';
            echo '<div id="'.$dev_id.'"  class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
        echo '<div style="clear:both">';
        echo '<center>'.$middle_ad.'</center>';    
        echo '</div>';
    }
   $i++;
}
?>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
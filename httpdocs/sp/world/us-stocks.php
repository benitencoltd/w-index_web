<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-us.js"></script>'
);

$active = 'word';
$sub_active = 'us-stocks';

include('../layout/header.php');
?>
<?php
$data=array();

$indexquote_title=array(
    25=>'AT&amp;T',
);

$usquote_title=array(
    2 => 'Google',
    3 => 'Yahoo!',
    4 => 'Ebay',
    5 => 'Amazon.com',
    6 => 'アドビシステムズ',
    7 => 'シマンテック',
    8 => 'オラクル',
    9 => 'テキサス・インスツルメンツ',
    10 => 'DELL',
    11 => 'NVIDIA',
    12 => 'AMD',
    13 => 'シティグループ',
    14 => 'ゴールドマン・サックス',
    15 => 'モルガン・スタンレー',
    16 => 'バンク・オブ・ニューヨーク',
    17 => 'バークシャー・ハザウェイ',
    18 => 'ゼネラル・モーターズ',
    19 => 'フォード・モーター',
    20 => 'スターバックス'
);

$i=0;
$re = mysql_query("SELECT * FROM dow30quote WHERE id IN (25) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $y['div']='us';
    $id=$y['id'];
    $title =$indexquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}



$re = mysql_query("select * from usquote where id IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $y['div']='us';
    $id=$y['id'];
    $title =$usquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}


function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];
    $result['title']=$data['title'];
    $result['last']=$data['last'];
    $result['div']=$data['div'];

    if($data['chan'] > 0){
        $result['detail']= "<span class='green'>+".$data['chan']."(+".$data['ratio']."%)</span>";
    }else{
        $result['detail']= "<span class='red'>".$data['chan']."(".$data['ratio']."%)</span>";
    }

    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
foreach($data as $key=>$value){
    echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
                echo $value['detail'];
            echo '</b>';
            echo '<div id="'.$dev_id.'" class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
       $i++;
}
?>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
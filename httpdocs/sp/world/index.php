<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-dow30.js"></script>'
);
$active='word';
$sub_active='word';
include('../layout/header.php');
?>
<?php
$data=array();

$dow30quote_title=array(
   4    => '<a href="/world/dow30/gs.html">ゴールドマン・サックス</a>',
   16   => '<a href="/world/dow30/jpmorgan.html">JPモルガン・チェース</a>',
   2    =>  '<a href="/world/dow30/amex.html">アメリカンエキスプレス</a>',
   26   =>  '<a href="/world/dow30/travelers.html">トラベラーズ</a>',
   10   =>  '<a href="/world/dow30/ge.html">ゼネラル・エレクトリック</a>',
   3    =>  '<a href="/world/dow30/boeing.html">ボーイング</a>',
   5    =>  '<a href="/world/dow30/caterpillar.html">キャタピラー</a>',
   27   =>  '<a href="/world/dow30/unitedtechnolgies.html">ユナイテッド・テクノロジーズ</a>',
   22   => '<a href="/world/dow30/microsoft.html">マイクロソフト</a>',
   14   => '<a href="/world/dow30/intel.html">インテル</a>',
   13   => '<a href="/world/dow30/ibm.html">IBM</a>',
   12   => '<a href="/world/dow30/visa.html">Visa</a>',
   6   => '<a href="/world/dow30/cisco.html">シスコシステムズ</a>',
   1   => '<a href="/world/dow30/apple.html">Apple</a>',
   28   => '<a href="/world/dow30/verizon.html">ベライゾン</a>',
   8   => '<a href="/world/dow30/dupont.html">デュポン</a>',
   30   => '<a href="/world/dow30/exxonmobil.html">エクソンモービル</a>',
   7   => '<a href="/world/dow30/chevron.html">シェブロン</a>',
   1   => '<a href="/world/dow30/nike.html">ナイキ</a>',
   20   => '<a href="/world/dow30/3m.html">スリーエム</a>',
   21   => '<a href="/world/dow30/merck.html">メルク</a>',
   23   => '<a href="/world/dow30/pfizer.html">ファイザー</a>',
   24   => '<a href="/world/dow30/p-g.html">P&amp;G</a>',
   15   => '<a href="/world/dow30/j-j.html">ジョンソン・エンド・ジョンソン</a>',
   29   => '<a href="/world/dow30/walmart.html">ウォルマート</a>',
   11   => '<a href="/world/dow30/homedepot.html">ホームデポ</a>',
   17   => '<a href="/world/dow30/unitedhealth.html">ユナイテッドヘルス</a>',
   19   => '<a href="/world/dow30/mcdonald.html">マクドナルド</a>',
   18   => '<a href="/world/dow30/cocacola.html">コカ・コーラ</a>',
   9   => '<a href="/world/dow30/disney.html">ウォルト・ディズニー</a>'
);

$i=0;
$re = mysql_query("SELECT * FROM dow30quote WHERE id IN (4,16,4,2,26,10,3,5,27,22,14,13,12,6,1,28,8,30,7,1,20,21,23,24,15,29,11,17,19,18,9) ORDER BY id");
while($y=mysql_fetch_array($re)){
  $y['div']='d';
    $id=$y['id'];
    $title =$dow30quote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}

function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];
    $result['title']=$data['title'];
    $result['last']=$data['last'];
    $result['div']=$data['div'];
    if($data['chan'] > 0){
        $result['detail']= "<span class='green'>+".$data['chan']."(+".$data['ratio']."%)</span>";
    }else{
        $result['detail']= "<span class='red'>".$data['chan']."(".$data['ratio']."%)</span>";
    }

    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
foreach($data as $key=>$value){
    echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
                echo $value['detail'];
            echo '</b>';
            echo '<div id="'.$dev_id.'" class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';
            echo '</div>';
        }
       $i++;
}
?>    
</div><!-- main -->

<?php
include('../layout/footer.php');
?>
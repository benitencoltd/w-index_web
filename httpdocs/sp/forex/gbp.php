<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-forex.js"></script>'
);

$active = 'forex';
$sub_active = 'gbp';

include('../layout/header.php');
?>
<?php
$data=array();

$forexquote_title=array(
    3   => array('1GBP','ポンド/円(GBP/JPY)','JPY'),
    9   => array('1GBP','ポンド/米ドル(GBP/USD)','USD'),
    12   => array('1EUR','ユーロ/ポンド(EUR/GBP)','GBP'),
    39   => array('1GBP','ポンド/豪ドル(GBP/AUD)','AUD'),
    40   => array('1GBP','ポンド/NZドル(GBP/NZD)','NZD'),
    41   => array('1GBP','ポンド/カナダドル(GBP/CAD)','CAD'),
    42   => array('1GBP','ポンド/スイスフラン(GBP/CHF)','CHF'),

);

$i=0;
$re = mysql_query("SELECT * FROM forexquote WHERE id IN (3,9,12,39,40,41,42)");
while($y=mysql_fetch_array($re)){
    $y['div'] =  'f';
    $id=$y['id'];
    $title =$forexquote_title[$id][1];
    $ex = $forexquote_title[$id][0];
    $y['title']=$title;
    $y['ex']=$ex;
    $y['ext'] = isset($forexquote_title[$id][2])?$forexquote_title[$id][2]:'';
    $data[$i] = getData($y);
    $i++;
}

function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];

    $result['title']=$data['title'];

    $result['ex']=$data['ex'];
    $result['ext']=$data['ext'];

    $result['last']=$data['last'];
    $result['div']=$data['div'];

    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
foreach($data as $key=>$value){
    echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart forex box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo $value['ex'].'=';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
            echo '</b>';
            echo $value['ext'];
            echo '<div id="'.$dev_id.'" class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
       $i++;
}
?>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
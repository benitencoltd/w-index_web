<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-forex.js"></script>'
);

$active = 'forex';
$sub_active = 'aud';

include('../layout/header.php');
?>
<?php
$data=array();

$forexquote_title=array(
    4   => array('1AUD','豪ドル/円(AUD/JPY)','JPY'),
    10   => array('1AUD','豪ドル/米ドル(AUD/USD)','USD'),
    31   => array('1EUR','ユーロ/豪ドル(EUR/AUD)','AUD'),
    39   => array('1GBP','ポンド/豪ドル(GBP/AUD)','AUD'),
    43   => array('1AUD','豪ドル/NZドル(AUD/NZD)','NZD'),
    44   => array('1AUD','豪ドル/カナダドル(AUD/CAD)','CAD'),
    45   => array('1AUD','豪ドル/スイスフラン(AUD/CHF)','CHF'),

);

$i=0;
$re = mysql_query("SELECT * FROM forexquote WHERE id IN (4,10,31,39,43,44,45)");
while($y=mysql_fetch_array($re)){
    $y['div']='f';
    $id=$y['id'];
    $title =$forexquote_title[$id][1];
    $ex = $forexquote_title[$id][0];
    $y['title']=$title;
    $y['ex']=$ex;
    $y['ext'] = isset($forexquote_title[$id][2])?$forexquote_title[$id][2]:'';
    $data[$i] = getData($y);
    $i++;
}

function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];

    $result['title']=$data['title'];

    $result['ex']=$data['ex'];
    $result['ext']=$data['ext'];

    $result['last']=$data['last'];
    $result['div']=$data['div'];

    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
foreach($data as $key=>$value){
    echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart forex box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo $value['ex'].'=';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
            echo '</b>';
            echo $value['ext'];
            echo '<div id="'.$dev_id.'"  class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
       $i++;
}
?>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
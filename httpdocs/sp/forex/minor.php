<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-forex.js"></script>'
);

$active = 'forex';
$sub_active = 'minor';

include('../layout/header.php');
?>
<?php
$data=array();

$forexquote_title=array(
    13  => array('1HKD','香港ドル/円(HKD/JPY)','JPY'),
    14  => array('1CNY','人民元/円(CNY/JPY)','JPY'),
    47  => array('1USD','米ドル/人民元(USD/CNY)','CNY'),
    15  => array('1KRW','韓国ウォン/円(KRW/JPY)','JPY'),
    16  => array('1THB','タイバーツ/円(THB/JPY)','JPY'),
    17  => array('1SGD','シンガポールドル/円(SGD/JPY)','JPY'),
    18  => array('1TRY','トルコリラ/円(TRY/JPY)','JPY'),
    19  => array('1EUR','ユーロ/デンマーククローネ','DKK'),
    20  => array('1EUR','ユーロ/ロシアルーブル(EUR/RUB)','RUB'),
    21  => array('1EUR','ユーロ/ルーマニアレウ(EUR/RON)',''),
    22  => array('1USD','米ドル/香港ドル(USD/HKD)','HKD'),
);

$i=0;
$re = mysql_query("SELECT * FROM forexquote WHERE id IN (13,14,47,15,16,17,18,19,20,21,22)");
while($y=mysql_fetch_array($re)){
    $y['div']='f';
    $id=$y['id'];
    $title =$forexquote_title[$id][1];
    $ex = $forexquote_title[$id][0];
    $y['title']=$title;
    $y['ex']=$ex;
    $y['ext'] = isset($forexquote_title[$id][2])?$forexquote_title[$id][2]:'';
    $data[$i] = getData($y);
    $i++;
}

function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];

    $result['title']=$data['title'];

    $result['ex']=$data['ex'];
    $result['ext']=$data['ext'];

    $result['last']=$data['last'];
    $result['div']=$data['div'];
    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
foreach($data as $key=>$value){
    echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart forex box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo $value['ex'].'=';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
            echo '</b>';
            echo $value['ext'];
            echo '<div id="'.$dev_id.'" class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
       $i++;
}
?>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
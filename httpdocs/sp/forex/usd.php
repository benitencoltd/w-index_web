<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="../js/js-forex.js"></script>'
);

$active = 'forex';
$sub_active = 'usd';

include('../layout/header.php');
?>
<?php
$data=array();

$forexquote_title=array(
    2   => array('1EUR','ユーロ/円(EUR/JPY)','JPY'),
    8   => array('1EUR','ユーロ/米ドル(EUR/USD)','USD'),
    12   => array('1EUR','ユーロ/ポンド(EUR/GBP)','GBP'),
    31   => array('1EUR','ユーロ/豪ドル(EUR/AUD)','AUD'),
    32   => array('1EUR','ユーロ/カナダドル(EUR/CAD)','CAD'),
    33   => array('1EUR','ユーロ/スイスフラン(EUR/CHF)','CHF'),
    34   => array('1EUR','ユーロ/ノルウェークローネ(EUR/NOK)','NOK'),
    46   => array('1EUR','ユーロ/NZドル(EUR/NZD)','NZD'),
    35   => array('1EUR','ユーロ/スウェーデンクローナ(EUR/SEK)','SEK'),
    36   => array('1EUR','ユーロ/ポーランドズロチ(EUR/PLN)','PLN'),
    37   => array('1EUR','ユーロ/シンガポールドル(EUR/SGD)','SGD'),
    38   => array('1EUR','ユーロ/トルコリラ(EUR/TRY)','TRY'),
    19   => array('1EUR','ユーロ/デンマーククローネ','DKK'),
    20   => array('1EUR','ユーロ/ロシアルーブル(EUR/RUB)','RUB'),
    21   => array('1EUR','ユーロ/ルーマニアレウ(EUR/RON)','RON'),

);

$i=0;
$re = mysql_query("SELECT * FROM forexquote WHERE id IN (2,8,12,31,32,33,34,46,35,36,37,38,19,20,21)");
while($y=mysql_fetch_array($re)){
    $y['div'] =  'f';
    $id=$y['id'];
    $title =$forexquote_title[$id][1];
    $ex = $forexquote_title[$id][0];
    $y['title']=$title;
    $y['ex']=$ex;
    $y['ext'] = isset($forexquote_title[$id][2])?$forexquote_title[$id][2]:'';
    $data[$i] = getData($y);
    $i++;
}

function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];

    $result['title']=$data['title'];

    $result['ex']=$data['ex'];
    $result['ext']=$data['ext'];

    $result['last']=$data['last'];
    $result['div']=$data['div'];

    return $result;
}
?>
<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
foreach($data as $key=>$value){
    echo "<div class='grid'>";
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart forex box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo $value['ex'].'=';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
            echo '</b>';
            echo $value['ext'];
            echo '<div id="'.$dev_id.'" class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){

                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                    })();

                </script>
            <?php
        echo '</div>';
    echo '</div>';
    if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
       $i++;
}
?>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
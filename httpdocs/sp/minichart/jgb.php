<?php
$url = '../../';

$active='minichart2';
$sub_active='jgb';
$reload_img=false;
include('../layout/header.php');
?>

<div id="main">
    <div class="image" id="image">
        <iframe id="frame" width='100%' height="650px" frameborder="0" scrolling="no"  allowtransparency="true" 
                src="http://tools.fxempire.com/live-charts.php?instrument=Japan+Govt+Bond&frequency=5_min&graph_type=candlestick&headline_color=082f60&text_color=333&bg_color=fff&frame_color=ccc"></iframe> 
    </div>
</div><!-- main -->

<script>
    var width = document.getElementById("image").clientWidth;
    width = width - 20;
    document.getElementById('frame').src = 'http://tools.fxempire.com/live-charts.php?instrument=Japan+Govt+Bond&frequency=5_min&graph_type=candlestick&width='+width+'&headline_color=082f60&text_color=333&bg_color=fff&frame_color=ccc';
</script>
<?php
include('../layout/footer.php');
?>
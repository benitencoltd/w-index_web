<?php
$url = '../../';

$active='minichart1';
$sub_active='nikkei-cfd';
$reload_img=false;
include('../layout/header.php');
?>

<div id="main">
    <div class="image">
    <script type="text/javascript">DukascopyApplet = {"type":"chart","params":{"showUI":false,"showTabs":false,"showParameterToolbar":true,"showOfferSide":true,"allowInstrumentChange":true,"allowPeriodChange":true,"allowOfferSideChange":true,"showAdditionalToolbar":true,"showExportImportWorkspace":true,"allowSocialSharing":true,"showDetachButton":false,"presentationType":"candle","axisX":true,"axisY":true,"legend":true,"timeline":false,"showDateSeparators":false,"showZoom":true,"showScrollButtons":true,"showAutoShiftButton":false,"crosshair":false,"borders":false,"freeMode":false,"theme":"Pastelle","uiColor":"#000","availableInstruments":"l:","instrument":"E_N225Jap","period":"5","offerSide":"BID","timezone":1,"live":true,"allowPan":true,"width":"100%","height":"100%","adv":"popup","lang":"jp"}};</script><script type="text/javascript" src="//freeserv.dukascopy.com/2.0/core.js"></script>
    </div>
</div><!-- main -->

<?php
include('../layout/footer.php');
?>
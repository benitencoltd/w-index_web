!function () {
    "undefined" != typeof YAHOO && YAHOO || (YAHOO = {}),
    void 0 !== YAHOO.JP && YAHOO.JP || (YAHOO.JP = {}),
    void 0 !== YAHOO.JP.fin && YAHOO.JP.fin || (YAHOO.JP.fin = {}),
    void 0 !== YAHOO.JP.fin.common && YAHOO.JP.fin.common || (
        YAHOO.JP.fin.common = {}
    )
}(),
function () {
    "use strict";
    var a = YAHOO.JP.fin.common;
    a.drawIncChart  = function (b, c, d, e) {
        
        "d" == e? a.drawSparklineChart(b, c, d) : a.drawNebulaChart(b, c, d, e)
    },
    a.drawSparklineChart = function (b, c, d) {
       
        var e,
            f,
            g;
        return null == (e = document.getElementById(b))
            ? (console.error("can not get element by elementId."), !1)
            : (
                f = document.createElement("canvas"),
                e.appendChild(f),
                (g = a.getSparklineParam(c, d)) === !1
                    ? (console.error("error when making sparkline param"), !1)
                    : "undefined" == typeof yfinanceSpark
                        ? (console.error("missing sparkline lib."), !1)
                        : void yfinanceSpark.init(g, f, function (d) {
                            d && a.drawErrorChartImg(b, c, size)
                        })
            )
    },
    a.drawNebulaChart            = function (b, c, d, e) {
      
        var f, g;
        f = document.getElementById(b)
        if(null == f){
            console.log("cannot get element by elementid")
        }else{
            g = a.getLibraParams(c,d,e)
           
            if(g === !1){
                console.log("error when making libra param");
            }else{
               
               
                if(typeof yfinanceLibra == "undefined"){
                    console.log("missing nebula lib");
                }else{
                   
                    void yfinanceLibra.init(g, f, function (d) {
                       
                        d && ( "spList" == e
                                ? a.drawErrorTextForSmartphone(b, !0)
                                : "spDetail" == e
                                    ? a.drawErrorTextForSmartphone(b, !1)
                                    : a.drawErrorChartImg(b, c, e)
                        )
                    })
                }
            }
        }
        return null == (f = document.getElementById(b))
            ? (console.error("can not get element by elementId."), !1)
            : (g = a.getLibraParams(c, d, e)) === !1
                ? (console.error("error when making libra param"), !1)
                : "undefined" == typeof yfinanceLibra
                    ? (console.error("missing nebula lib."), !1)
                    : void yfinanceLibra.init(g, f, function (d) {
                        d && (
                            "spList" == e
                                ? a.drawErrorTextForSmartphone(b, !0)
                                : "spDetail" == e
                                    ? a.drawErrorTextForSmartphone(b, !1)
                                    : a.drawErrorChartImg(b, c, e)
                        )
                    })
                    
    },
    a.drawErrorChartImg    = function (a, b, c) {
        var d,
            e,
            f;
        if (null == (d = document.getElementById(a))) 
            return console.error("can not get element by elementId."),
            !1;
        for (; d.firstChild;) 
            d.removeChild(d.firstChild);
       
        return f = "https://chart.yahoo.co.jp/?code=" + b + "&tm=1d&size=" + c,
        e        = document.createElement("img"),
        e.setAttribute("src", f),
        d.appendChild(e),
        !0
    },
    a.drawErrorTextForSmartphone = function (a, b) {
        var c,
            d,
            e,
            f;
        return e = "通信エラーのためチャートが表示できません",
        null == (c = document.getElementById(a))
            ? (console.error("can not get element."), !1)
            : (
                d           = c,
                1 == b && (f = c.parentNode, f.removeChild(c), d = f),
                d.innerText = e,
                d.classList.add("noChart"),
                !0
            )
    },
    a.getSparklineParam          = function (a, b) {
        return null == a || null == b
            ? (console.error("some argments are null."), !1)
            : {
                area  : !1,
                h     : 16,
                range : b,
                symbol: a,
                w     : 60
            }
    },
    a.getLibraParams    = function (a, b, c) {
        //console.log(a+" "+b+" "+c)
        var d,
            e,
            f = !1;
        if (null == a || null == b || null == c) 
            return console.error("some argments are null."),
            !1;
        switch (c) {
            case "n":
                d = 630,
                e = 273,
                f = !0;
                break;
            case "m":
                d = 512,
                e = 222,
                f = !0;
                break;
            case "e":
                d = 290,
                e = 140;
                break;
            case "c":
                d = 240,
                e = 120;
                break;
            case "b":
                d = 192,
                e = 96;
                break;
            case "h":
                d = 120,
                e = 90;
                break;
            case "spList":
                d = 300,
                e = 170;
                break;
            case "spDetail":
                d = 300,
                e = 210,
                f = !0;
                break;
            default:
                d = 240,
                e = 120
        }
        return {
            h              : e,
            locale         : "ja",
            range          : b,
            showPriceLabels: !1,
            symbol         : a,
            ui             : {
                trendDownColor: "#0082e6",
                trendUpColor  : "#0082e6"
            },
            volume         : f,
            w              : d
        }
    }
}();
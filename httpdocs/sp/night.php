<?php
$js_script = array(
    '<script type="text/javascript" src="js/js-night.js"></script>'
);
$active = 'night';
include('layout/header.php');
?>
<?php
$data=array();

$indexquote_title=array(
    3=>'S&amp;P500',
    12=>'イギリス FTSE100',
    13=>'フランス CAC40',
    14=>'ドイツ DAX',
);

$otherquote_title=array(
    3=>'米10年国債',
    5=>'RUSSELL2000',
    6=>'AMEX総合指数',
    9=>'DJ EU STOXX50',
    8=>'VIX指数(恐怖指数)',
);

$comquote_title=array(
    2=>'WTI原油先物',
    25=>'DJ-UBS商品指数',
    8=>'SOXX 半導体株指数',
);

$i=0;
$re = mysql_query("SELECT * FROM indexquote WHERE id IN (3) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $id=$y['id'];
    $y['div']='i';
    $title =$indexquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}



$re = mysql_query("select * from otherquote where id IN (3,5,6,9) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $y['div']='o';
    $id=$y['id'];
    $title =$otherquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}

$re = mysql_query("SELECT * FROM indexquote WHERE id IN (12,13,14) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $id=$y['id'];
    $y['div']='i';
    $title =$indexquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}

$re = mysql_query("select * from comquote where id IN(2)");
while($y=mysql_fetch_array($re)){
    $id=$y['id'];
    $y['div']='c';
    $title =$comquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}

$re = mysql_query("select * from otherquote where id IN (8) ORDER BY id");
while($y=mysql_fetch_array($re)){
    $id=$y['id'];
    $y['div']='o';
    $title =$otherquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}

$re = mysql_query("select * from comquote where id IN (25,8)");
while($y=mysql_fetch_array($re)){
    $id=$y['id'];
    $y['div']='c';
    $title =$comquote_title[$id];
    if($id==8){
        $title='SOXX 半導体株指数';
    }

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}


function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];
    $result['title']=$data['title'];
    $result['last']=$data['last'];
    $result['div']=$data['div'];
    if($data['chan'] > 0){
        $result['detail']= "<span class='green'>+".$data['chan']."(+".$data['ratio']."%)</span>";
    }else{
        $result['detail']= "<span class='red'>".$data['chan']."(".$data['ratio']."%)</span>";
    }

    return $result;
}
?>

<div id="tops">
    <div class="grid">
        <div class="nydow box">
            <h3>NYダウ30</h3>
            <a href="https://www.google.com/finance?q=INDEXDJX:.DJI&ntsp=0" class="highslide" target="_blank" >
                <img src="http://w-index.org/cimg/dow30.gif"  class="imup" alt="NYダウチャート" />
            </a>
        </div>
    </div>
    <div class="grid">
        <div class="nydow box">
            <h3>NASDAQ</h3>
            <a href="https://www.google.com/finance?q=INDEXNASDAQ:.IXIC" class="highslide" target="_blank" >
                <img src="http://w-index.org/cimg/nasdaq.gif" class="imup" alt="NASDAQチャート" />
            </a>
        </div>
    </div>
    <div class="grid">
        <div class="usdjpy box">
            <h3>米ドル/円</h3>
            <a href="https://www.google.com/finance?q=USDJPY" class="highslide" target="_blank">
                <img src="http://w-index.org/cimg/usdjpy.gif" class="imup" alt="USD/JPYチャート" />
            </a>
        </div>
    </div>
    <div class="grid">
        <div class="usdjpy box">
            <h3>ユーロ/米ドル</h3>
            <a href="https://www.google.com/finance?q=EURUSD" class="highslide" target="_blank" >
                <img src="http://w-index.org/cimg/eurusd.gif" class="imup" alt="EUR/USDチャート" />
            </a>
        </div>
    </div>
</div><!-- tops -->

<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
    
    foreach($data as $key=>$value){
        echo '<div class="grid">';
            $dev_id = 'chartid-'.$key;
            echo '<div class="wchart box fullChart" data-symbol="'.$value['symbol'].'">';
                echo '<h3>'.$value['title'].'</h3>';
                echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                    echo $value['last'];
                    echo $value['detail'];
                echo '</b>';
                echo '<div id="'.$dev_id.'"  class="chart" data-symbol="'.$value['symbol'].'">';
                echo '</div>';
                ?>
                    <script>
                        (function(){

                        YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");

                        })();

                    </script>
                <?php
            echo '</div>';
        echo '</div>';
        if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
       $i++;
    }
    ?>
</div><!-- main -->

<?php
include('layout/footer.php');
?>
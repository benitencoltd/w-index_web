<?php
$url = '../../';
$active = 'support';
include('../layout/header.php');
?>

<div id="main">
    <h1>【お探しのページは見つかりませんでした。】</h1>
    <div class="note">
    <p>お探しのページは見つかりませんでした。</p>

    <p><a href="/sp/">世界の株価指数はこちらです。</a></p>

    <p><a href="/sp/night.php">欧州の株価（夜間用）はこちらです。</a></p>

    <p><a href="/old/">旧バージョンのページはこちらです。</a></p>

    <p>
    右上のサイト内検索もご活用ください。
    </p>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
<?php
$url = '../../';
$active = 'support';
include('../layout/header.php');
?>

<div id="main">
    <div class='content'>
        <h1>【当サイトについて】</h1>
        <div class="note">
        <p><b>サイト名</b><br />
        リアルタイム世界の株価指数と為替</p>

        <p><b>サイトURL</b><br />
        <a href="/sp/">http://www.w-index.com/</a>

        <p><b>運営開始</b><br />
        2003年9月</p>

        <p><b>概要</b><br />
        NYダウ、ナスダックをはじめとする世界の株価指数を見ることができるサイトです。<br />
        現在の世界の株式市場や個別銘柄、外国為替がどのように動いているのかわかります。<br />
        ※当サイトの情報は、ディレイなし～最大約20分遅れで配信されています。</p>

        <p><b>主な情報元</b></p>

        <ul>
        <li><a href="http://www.yahoo.com/" target="_blank">Yahoo!</a></li>
        <li><a href="http://www.yahoo.co.jp/" target="_blank">Yahoo!Japan</a></li>
        <li><a href="http://www.cmegroup.com/" target="_blank">CME Group</a></li>
        <li><a href="http://futuresource.quote.com/" target="_blank">futuresource.com</a></li>
        <li><a href="http://bigcharts.marketwatch.com/" target="_blank">BigCharts</a></li>
        <li><a href="http://www.kitco.com/" target="_blank">Kitco</a></li>
        </ul>

        <p>当サイトにて掲載している一部情報の著作権は各権利所有者に帰属します。</p>


        <p><b>ご注意</b><br />
        チャートの遅延、データの配信遅延、情報の間違い等をはじめとするトラブルが起こりえることをご了承ください。<br />
        また、当サイトはこれらの情報を用いて行う判断の一切について責任を負うものではありません。</p>

        <p>何かございましたら<a href="/sp/support/contact.php">お問い合わせフォーム</a>もしくは下記メールアドレスよりご連絡下さい。</p>

        <p>連絡先メールアドレス<br />
        support■w-index.com　（■を@に変更して下さい）</p>
    </div>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
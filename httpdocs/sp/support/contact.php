<?php
$url = '../../';
$active = 'support';
include('../layout/header.php');
?>

<div id="main">
    <p>ご意見ご感想、バグの報告等、管理者への連絡の際にお使い下さい。</p>

    <p>お問い合わせは、下記メールアドレスへメールにてご連絡ください。</p>

    <p>お問い合わせ先メールアドレス</p>
    <p>support■w-index.com　（■を@に変更して下さい）</p>
</div><!-- main -->


<?php
include('../layout/footer.php');
?>
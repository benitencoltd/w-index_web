<?php
$js_script=array('<script type="text/javascript" src="js/js.js"></script>');
$active = 'top';
include('layout/header.php');

?>
<?php
$data=array();
$indexquote_title=array(
    4=>'日経225',
    5=>'韓国 KOSPI',
    6=>'中国 上海総合',
    7=>'台湾 加権',
    8=>'香港　HANG SENG',
    9=>'シンガポール ST',
    10=>'オ-ストラリア ASX',
    11=>'インド SENSEX30',
    12=>'イギリス FTSE100',
    13=>'フランス CAC40',
    14=>'ドイツ DAX',
    15=>'イタリア FTSE MIB',
    16=>'スペイン IBEX35',
    17=>'オランダ AEX',
    18=>'カナダ S&amp;P TSX',
    19=>'メキシコ IPC',
    20=>'ブラジル Bovespa',
    21=>'アルゼンチン Merval',
    22=>'スイス SMI',
    23=>'ロシア RTSI',
 );
$re = mysql_query("SELECT * FROM indexquote WHERE id IN (4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23) ORDER BY id");
$i=0;
while($y=mysql_fetch_array($re)){
  $y['div']='i';

    $id=$y['id'];
    $title =$indexquote_title[$id];

    $y['title']=$title;
    $data[$i] = getData($y);
    $i++;
}

function getData($data){
    $result['id']=$data['id'];
    $result['symbol']=$data['name'];
    $result['title']=$data['title'];
    $result['last']=$data['last'];
    $result['div']=$data['div'];
    if($data['chan'] > 0){
        $result['detail']= "<span class='green'>+".$data['chan']."(+".$data['ratio']."%)</span>";
    }else{
        $result['detail']= "<span class='red'>".$data['chan']."(".$data['ratio']."%)</span>";
    }

    return $result;
}
?>
<div id="tops">
    <div class="grid">
        <div class="nydow box">
            <h3>NYダウ30</h3>
            <a href="https://www.google.com/finance?q=INDEXDJX:.DJI&ntsp=0" class="highslide" target="_blank" >
                <img src="http://w-index.org/cimg/dow30.gif"  class="imup" alt="NYダウチャート" />
            </a>
        </div>
    </div>
    <div class="grid">
        <div class="nydow box">
            <h3>NASDAQ</h3>
            <a href="https://www.google.com/finance?q=INDEXNASDAQ:.IXIC" class="highslide" target="_blank" >
                <img src="http://w-index.org/cimg/nasdaq.gif" class="imup" alt="NASDAQチャート" />
            </a>
        </div>
    </div>
    <div class="grid">
        <div class="usdjpy box">
            <h3>米ドル/円</h3>
            <a href="https://www.google.com/finance?q=USDJPY" class="highslide" target="_blank">
                <img src="http://w-index.org/cimg/usdjpy.gif" class="imup" alt="USD/JPYチャート" />
            </a>
        </div>
    </div>
    <div class="grid">
        <div class="usdjpy box">
            <h3>ユーロ/米ドル</h3>
            <a href="https://www.google.com/finance?q=EURUSD" class="highslide" target="_blank" >
                <img src="http://w-index.org/cimg/eurusd.gif" class="imup" alt="EUR/USDチャート" />
            </a>
        </div>
    </div>
</div><!-- tops -->

<div id="main">
    <?php
    $i=1;
    $num = intval(count($data) / 2) ;
    $num_ad = ($num % 2 ==0)? $num:$num+1 ;
    foreach($data as $key=>$value){
        echo '<div class="grid">';
        $dev_id = 'chartid-'.$key;
        echo '<div class="wchart box fullChart" data-symbol="'.$value['symbol'].'">';
            echo '<h3>'.$value['title'].'</h3>';
            echo '<b id="'.$value['div'].'-'.$value['id'].'">';
                echo $value['last'];
                echo $value['detail'];
            echo '</b>';
            echo '<div id="'.$dev_id.'" class="chart" data-symbol="'.$value['symbol'].'">';
            echo '</div>';
            ?>
                <script>
                    (function(){
                    YAHOO.JP.fin.common.drawIncChart("<?php echo $dev_id; ?>", "<?php echo $value['symbol']; ?>", "1d", "b");
                    })();
                </script>
            <?php
        echo '</div>';
        echo '</div>';
        if($i == $num_ad){
            echo '<div style="clear:both">';
            echo '<center>'.$middle_ad.'</center>';    
            echo '</div>';
        }
        $i++;
    }
    ?>
</div><!-- main -->
<?php
include('layout/footer.php');
?>
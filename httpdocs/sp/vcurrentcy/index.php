<?php
$url = '../../';
$js_script = array(
    '<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>'
);
$active='vcurrentcy';
$reload_img=false;
include('../layout/header.php');

$symbols = array(
    'BITFLYER:BTCJPY',
    'POLONIEX:ETHBTC*BITFLYER:BTCJPY',
    'POLONIEX:XRPBTC*BITFLYER:BTCJPY',
    'POLONIEX:LTCBTC*BITFLYER:BTCJPY',
    'POLONIEX:DASHBTC*BITFLYER:BTCJPY',
    'POLONIEX:FCTBTC*BITFLYER:BTCJPY',
    'POLONIEX:XMRBTC*BITFLYER:BTCJPY',
    'POLONIEX:BTSBTC*BITFLYER:BTCJPY',
    'POLONIEX:STRBTC*BITFLYER:BTCJPY',
    'POLONIEX:CLAMBTC*BITFLYER:BTCJPY',
    'POLONIEX:DOGEBTC*BITFLYER:BTCJPY',
    'POLONIEX:MAIDBTC*BITFLYER:BTCJPY',
);
?>

<div id="main">
    <!-- TradingView Widget BEGIN -->
    <?php 
    $i=1;
    $n=1;
    foreach($symbols as $symbol){
    ?>
        <div class="grid">
            <!-- TradingView Widget BEGIN -->
            <script type="text/javascript">
            new TradingView.widget({
              "autosize": true,
              "symbol": "<?php echo $symbol;?>",
              "interval": "D",
              "timezone": "Asia/Tokyo",
              "theme": "White",
              "style": "1",
              "locale": "en",
              "toolbar_bg": "#f1f3f6",
              "enable_publishing": false,
              "hide_top_toolbar": true,
              "allow_symbol_change": true,
              "save_image": false,
              "hideideas": true
            });
            
            </script>

        </div>
        <?php
       
        if($i % 6 ==0 && $i <12 && $n<=6){
            echo '<center>'.$middle_ad.'</center>';   
            $n++;
        }
        $i++;
    }
?>
    </div>
<!-- TradingView Widget END -->

</div><!-- main -->
<script>
    $(document).ready(function(){
        var d_height=425;
        var d_width=490;
        var thumb_width = $('.grid').width();
        var res_height = (thumb_width * d_height) / d_width;
        
        $(".grid").height(res_height);
    })
</script>
<?php
include('../layout/footer.php');
?>
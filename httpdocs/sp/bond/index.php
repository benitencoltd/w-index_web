<?php
$url='../../';
$js_script=array(
    '<script type="text/javascript" src="../js/js-bond.js"></script>'
);
$active = 'bond';
$reload_img=false;
include('../layout/header.php');
?>
<div id="main-forex">
    <h2>日本長期国債先物チャート（JGB10年債）</h2>

    <!--FX Empire Forex Tools BEGIN-->                    
    <iframe id="forx" width="100%" height="620" frameborder="0" scrolling="no" allowtransparency="true" 
        src="http://tools.fxempire.com/live-charts.php?instrument=Japan+Govt+Bond&frequency=5_min&graph_type=candlestick&width=740&height=620&headline_color=082f60&text_color=333&bg_color=fff&frame_color=ccc">
    </iframe>              
    <div class="col-xs-12">
    <div id="fxempire_link">                        
        The Free Charts are Powered by 
        <a rel="nofollow" style="text-decoration: underline;" target="_blank" href="http://www.fxempire.com">
            FXEmpire.com</a> - Your Leading Financial Portal
    </div>
        <!--FX Empire Forex Tools END-->

        <h2>米10年国債先物チャート（Globex）</h2>
        <img src="http://w-index.org/bond/t-note.png"  class="imup" alt="米国長期10年国債5分足チャート" />
    </div>
</div><!-- main -->
<script>
    var width = document.getElementById("main-forex").clientWidth;
    document.getElementById('forx').src = 'http://tools.fxempire.com/live-charts.php?instrument=Japan+Govt+Bond&frequency=5_min&graph_type=candlestick&width='+width+'&height=620&headline_color=082f60&text_color=333&bg_color=fff&frame_color=ccc';
</script>
<?php
include('../layout/footer.php');
?>
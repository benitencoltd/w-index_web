<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>NYダウ工業株30銘柄チャート | リアルタイム世界の株価指数と為替</title>
<meta name="description" content="アメリカのNYダウ工業株30種採用の個別銘柄チャートです。" />
<meta name="keywords" content="NYダウ,チャート,30,銘柄" />
<link rel="stylesheet" href="/css/styles.css" type="text/css" />
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if(eregi('Mac', $ua)){
	echo('<link rel="stylesheet" href="/css/stylesm.css" type="text/css" />');
}
?>
<script type="text/javascript" src="/js/js-dow30.js"></script>
<link rel="shortcut icon" href="/img/favicon.ico" />
<meta name="format-detection" content="telephone=no" />

<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'ja'}
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36649344-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<!-- 開発用のinclude -->
<!--<?php
require_once("../../private/dbinfo.php");
$s=mysql_connect($serv,$user,$pass) or die("取得失敗");
mysql_select_db($dbnm);
?> -->
<!-- End -->

<!-- <?php/*
require_once("/var/www/vhosts/w-index.com/private/dbinfo.php");
$s=mysql_connect($serv,$user,$pass) or die("取得失敗");
mysql_select_db($dbnm);*/
?> -->
<body onload ="setInterval('update_images()',1000*60);">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="container">
<div id="header">
<a href="http://www.w-index.com/"><img src="/img/logo.png" id="himg" width="241" height="28" alt="リアルタイム世界の株価指数と為替" /></a>
<ul id="headerright">
<li><b>S&amp;P500</b>：<?php
$re = mysql_query("select last,chan from indexquote where id='3'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span>";}
else{echo "<span class=\"green\">+$y[1]</span>";}
}
?></li>
<li><b>WTI原油</b>：<?php
$re = mysql_query("select last,chan from comquote where id='2'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span>";}
else{echo "<span class=\"green\">+$y[1]</span>";}
}
?></li>
<li><b>金(Gold)</b>：<?php
$re = mysql_query("select last,chan from comquote where id='6'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span>";}
else{echo "<span class=\"green\">+$y[1]</span>";}
}
?></li>
<li><a href="/old/">旧デザインにする</a></li>
</ul>
</div><!-- header -->

<div id="bigbanner">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:header_728x90 */
google_ad_slot = "4499058204";
google_ad_width = 728;
google_ad_height = 90;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<noscript>
<a href="http://twitter.com/w_index" target="_blank"><img src="/img/tw72890.png" width="728" height="90" alt="世界の株価Twitter" /></a>
</noscript>
</div>

<div id="navi">
<ul>
<li><a href="/">トップ</a></li>
<li><a href="/night.html">夜間用</a></li>
<li><a href="/world/" class="select">外国株</a></li>
<li><a href="/forex/">外国為替</a></li>
<li><a href="/commodity/">商品相場</a></li>
<li><a href="/225/">日本株</a></li>
<li><a href="/bond/">国債</a></li>
<li><a href="/news/">ニュース</a></li>
<li><a href="/hikaku/">証券比較</a></li>
<li><a href="/pro/">専門家Q&amp;A</a></li>
</ul>

<form action="http://www.w-index.com/result/" id="cse-search-box">
  <div>
    <input type="hidden" name="cx" value="partner-pub-6205980071848979:1545591800" />
    <input type="hidden" name="cof" value="FORID:10" />
    <input type="hidden" name="ie" value="UTF-8" />
    <input type="text" name="q" size="22" />
    <input type="submit" name="sa" value="検索" />
  </div>
</form>
<script type="text/javascript" src="http://www.google.co.jp/coop/cse/brand?form=cse-search-box&amp;lang=ja"></script>

</div><!-- navi -->

<div class="subnavi">
<ul>
<li class="first"><a href="/world/" class="thispage">NYダウ30銘柄</a></li>
<li><a href="/world/us-stocks.html">米国主要銘柄</a></li>
<li><a href="/world/adr.html">日本株ADR</a></li>
<li><a href="http://dynamic.nasdaq.com/dynamic/premarketma.stm" class="blank" target="_blank">Nasdaq時間外 寄り前</a></li>
<li><a href="http://dynamic.nasdaq.com/dynamic/afterhourma.stm" class="blank" target="_blank">引け後</a></li>
<li><a href="http://finance.yahoo.com/q/cp?s=%5Endx" class="blank" target="_blank">Nasdaq100構成銘柄</a></li>
</ul>
</div><!-- subnavi -->

<h1>【NYダウ構成30銘柄チャート】</h1>
<div id="main">
<div class="wchart">
<h3><a href="/world/dow30/boa.html">バンク・オブ・アメリカ</a></h3>
<b id="d-4"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='4'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/bac.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/bac.png" width="190" height="95" class="imup" alt="バンク・オブ・アメリカ" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/jpmorgan.html">JPモルガン・チェース</a></h3>
<b id="d-16"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='16'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/jpm.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/jpm.png" width="190" height="95" class="imup" alt="JPモルガン・チェース" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/amex.html">アメリカンエキスプレス</a></h3>
<b id="d-2"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='2'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/axp.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/axp.png" width="190" height="95" class="imup" alt="アメリカンエキスプレス" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/travelers.html">トラベラーズ</a></h3>
<b id="d-26"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='26'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/trv.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/trv.png" width="190" height="95" class="imup" alt="トラベラーズ" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/ge.html">ゼネラル・エレクトリック</a></h3>
<b id="d-10"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='10'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/ge.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/ge.png" width="190" height="95" class="imup" alt="ゼネラル・エレクトリック" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/boeing.html">ボーイング</a></h3>
<b id="d-3"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='3'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/ba.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/ba.png" width="190" height="95" class="imup" alt="ボーイング" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/caterpillar.html">キャタピラー</a></h3>
<b id="d-5"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='5'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/cat.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/cat.png" width="190" height="95" class="imup" alt="キャタピラー" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/unitedtechnolgies.html">ユナイテッド・テクノロジーズ</a></h3>
<b id="d-27"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='27'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/utx.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/utx.png" width="190" height="95" class="imup" alt="ユナイテッド・テクノロジーズ" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/microsoft.html">マイクロソフト</a></h3>
<b id="d-22"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='22'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/msft.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/msft.png" width="190" height="95" class="imup" alt="マイクロソフト" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/intel.html">インテル</a></h3>
<b id="d-14"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='14'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/intc.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/intc.png" width="190" height="95" class="imup" alt="インテル" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/ibm.html">IBM</a></h3>
<b id="d-13"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='13'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/ibm.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/ibm.png" width="190" height="95" class="imup" alt="IBM" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/hp.html">ヒューレット・パッカード</a></h3>
<b id="d-12"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='12'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/hpq.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/hpq.png" width="190" height="95" class="imup" alt="ヒューレット・パッカード" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/cisco.html">シスコシステムズ</a></h3>
<b id="d-6"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='6'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/csco.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/csco.png" width="190" height="95" class="imup" alt="シスコシステムズ" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/at-t.html">AT&amp;T</a></h3>
<b id="d-25"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='25'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/t.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/t.png" width="190" height="95" class="imup" alt="AT&amp;T" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/verizon.html">ベライゾン</a></h3>
<b id="d-28"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='28'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/vz.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/vz.png" width="190" height="95" class="imup" alt="ベライゾン" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/dupont.html">デュポン</a></h3>
<b id="d-8"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='8'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/dd.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/dd.png" width="190" height="95" class="imup" alt="デュポン" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/exxonmobil.html">エクソンモービル</a></h3>
<b id="d-30"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='30'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/xom.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/xom.png" width="190" height="95" class="imup" alt="エクソンモービル" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/chevron.html">シェブロン</a></h3>
<b id="d-7"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='7'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/cvx.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/cvx.png" width="190" height="95" class="imup" alt="シェブロン" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/alcoa.html">アルコア</a></h3>
<b id="d-1"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='1'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/aa.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/aa.png" width="190" height="95" class="imup" alt="アルコア" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/3m.html">スリーエム</a></h3>
<b id="d-20"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='20'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/mmm.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/mmm.png" width="190" height="95" class="imup" alt="スリーエム" /></a>
</div>



<div class="wchart">
<h3><a href="/world/dow30/merck.html">メルク</a></h3>
<b id="d-21"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='21'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/mrk.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/mrk.png" width="190" height="95" class="imup" alt="メルク" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/pfizer.html">ファイザー</a></h3>
<b id="d-23"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='23'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/pfe.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/pfe.png" width="190" height="95" class="imup" alt="ファイザー" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/p-g.html">P&amp;G</a></h3>
<b id="d-24"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='24'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/pg.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/pg.png" width="190" height="95" class="imup" alt="P&amp;G" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/j-j.html">ジョンソン・エンド・ジョンソン</a></h3>
<b id="d-15"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='15'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/jnj.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/jnj.png" width="190" height="95" class="imup" alt="ジョンソン・エンド・ジョンソン" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/walmart.html">ウォルマート</a></h3>
<b id="d-29"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='29'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/wmt.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/wmt.png" width="190" height="95" class="imup" alt="ウォルマート" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/homedepot.html">ホームデポ</a></h3>
<b id="d-11"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='11'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/hd.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/hd.png" width="190" height="95" class="imup" alt="ホームデポ" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/kraftfoods.html">ユナイテッドヘルス</a></h3>
<b id="d-17"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='17'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/unh.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/unh.png" width="190" height="95" class="imup" alt="ユナイテッドヘルス" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/mcdonald.html">マクドナルド</a></h3>
<b id="d-19"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='19'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/mcd.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/mcd.png" width="190" height="95" class="imup" alt="マクドナルド" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/cocacola.html">コカ・コーラ</a></h3>
<b id="d-18"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='18'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/ko.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/ko.png" width="190" height="95" class="imup" alt="コカ・コーラ" /></a>
</div>

<div class="wchart">
<h3><a href="/world/dow30/disney.html">ウォルト・ディズニー</a></h3>
<b id="d-9"><?php
$re = mysql_query("select last,chan,ratio from dow30quote where id='9'");
while($y=mysql_fetch_array($re)){
echo "$y[0] ";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]($y[2]%)</span>";}
else{echo "<span class=\"green\">+$y[1](+$y[2]%)</span>";}
}
?></b>
<a href="http://w-index.org/world/dow30/daily/dis.png" class="highslide" onclick="return hs.expand(this)"><img src="http://w-index.org/world/dow30/dis.png" width="190" height="95" class="imup" alt="ウォルト・ディズニー" /></a>
</div>

<div id="underbanner">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:footer_728x90 */
google_ad_slot = "7452524605";
google_ad_width = 728;
google_ad_height = 90;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<noscript>
<a href="/land/click-sec.html" target="_blank" rel="nofollow"><img src="http://www.accesstrade.net/at/r.html?rk=01002rnn007j86" width="728" height="90" alt="クリック証券" /></a>
</noscript>
</div>

</div><!-- main -->

<div id="subwaku">

<div class="sub">
<h3>NYダウ30採用銘柄</h3>
<p class="pside">全て60秒間隔で自動更新します。U.S.東部時間表示です</p>
<p class="pside">銘柄名をクリックすると個別銘柄の解説が見られます。</p>
<p class="pside">チャートをクリックすると日足チャートが出ます。</p>
<p class="pside"><a href="/world/us-stocks.html">米国主要銘柄はこちらです（アップル・グーグル etc）</a></p>
<ul class="side">
<li><a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="w_index" data-lang="ja">ツイート</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script></li>
<li><div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div></li>
<li><g:plusone size="medium"></g:plusone></li>
</ul>
</div>

<div class="sub" id="quotemini">
<h3><a href="/">世界の株価</a></h3>
<table class="quote-mini">
<tr class="guusuu"><td class="td1">NASDAQ</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='2'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr><td class="td1">日経平均</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='4'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr class="guusuu"><td class="td1">中国</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='6'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr><td class="td1">ST</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='9'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr class="guusuu"><td class="td1">インド</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='11'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr><td class="td1">イギリス</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='12'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr class="guusuu"><td class="td1">フランス</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='13'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
<tr><td class="td1">ドイツ</td><td class="td2"><?php
$re = mysql_query("select last,chan from indexquote where id='14'");
while($y=mysql_fetch_array($re)){
echo "$y[0]","</td><td class=\"td3\">";
if(0 > $y[1]){echo "<span class=\"red\">$y[1]</span></td></tr>";}
else{echo "<span class=\"green\">+$y[1]</span></td></tr>";}
}
?>
</table>

</div>

<div class="sub">
<h3><a href="/outside/blank.html">CME GLOBEX</a></h3>
<ul class="side">
<li><a href="/minichart/ny-dow.html" onclick="open('/minichart/ny-dow.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">NYダウ先物</a></li>
<li><a href="/minichart/nasdaq.html" onclick="open('/minichart/nasdaq.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">NASDAQ先物</a></li>
<li><a href="/minichart/sp500.html" onclick="open('/minichart/sp500.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">S&amp;P500先物</a></li>
<li><a href="/minichart/cme-nikkei225.html" onclick="open('/minichart/cme-nikkei225.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">CME Nikkei225</a></li>
<li><a href="/minichart/cme-nikkei225-yen.html" onclick="open('/minichart/cme-nikkei225-yen.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">CME Nikkei225(円建て)</a></li>
<li><a href="/minichart/crude-oil.html" onclick="open('/minichart/crude-oil.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">WTI原油先物</a></li>
<li><a href="/minichart/gold.html" onclick="open('/minichart/gold.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">金(Gold)</a></li>
</ul>
</div>

<div class="sub">
<h3>主要指標</h3>
<ul class="side">
<li><a href="/minichart/sgx-nikkei225.html" onclick="open('/minichart/sgx-nikkei225.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">SGX Nikkei225</a></li>
<li><a href="/minichart/jgb.html" onclick="open('/minichart/jgb.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">日本国債(JGB)</a></li>
<li><a href="/minichart/vix.html" onclick="open('/minichart/vix.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">VIX指数</a></li>
<li><a href="/minichart/soxx.html" onclick="open('/minichart/soxx.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">SOXX半導体指数</a></li>
<li><a href="/minichart/crb.html" onclick="open('/minichart/crb.html','icq','width=650,height=605,left=300,top=50,scrollbars=1');return false">CRB指数</a></li>
</ul>
</div>

<div class="sub">
<div class="linkunit">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6205980071848979";
/* WI:right_link_120x90 */
google_ad_slot = "2743123400";
google_ad_width = 120;
google_ad_height = 90;
google_ad_region = "iid";
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
</div>

<div class="tweet">
<a href="http://www.twitter.com/w_index" target="_blank"><img src="/img/follow-me.png" width="160" height="36" alt="w_indexをフォロー"/></a>
</div>

</div><!-- subwaku -->

<div id="footer">
<div id="copyright">&copy;2003-<?php echo date(Y); ?> <a href="/">リアルタイム世界の株価指数と為替</a></div>
<ul>
<li><a href="/old/">旧デザインにする</a></li>
<li><a href="/support/contact.html">お問い合わせ</a></li>
<li><a href="/support/media.html">広告掲載について</a></li>
<li><a href="/support/about.html">当サイトについて</a></li>
</ul>
</div><!-- footer -->
</div><!-- containar -->

<?php
mysql_close($s);
?>

</body>
</html>